# README #

Web app conbining python and javascript for web collaborative app for sharing web sources. see dp.jirisemmler.eu

### Demo app and demo accounts ###
* app
* * http://dp.jirisemmler.eu/
* * User name: dp.test.semmler@gmail.com
* * User pass: sherito
* Email account for testing
* * Url: https://email.seznam.cz/
* * User name: dp-test@seznam.cz
* * User pass: sherito
* GoogleDrive account
* * User name: dp.test.semmler@gmail.com
* * User pass: sherito1993
* Git test repository
* * https://bitbucket.org/dp-sherito/dp-test1/overview
* * User name: dp.test.semmler@gmail.com
* * User pass: sherito1993

### Install ###

1. clone repository
1. config config.sh
1. run ./run.sh install
1. config dp/settings.py
1. run ./run.sh reset to init data
1. run ./run.sh setcron to set cronjobs
1. run bower install


### Any questions or cookies? ###

if you would like to offer me a cookie or a beer - feel free to contact me on jirka.semmler@gmail.com