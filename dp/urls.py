"""dp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from registration.backends.hmac.views import RegistrationView

from sherito import views
from sherito.api.git_api import GitAPI
from sherito.views import *

urlpatterns = [
    url(r'^$', views.index),
    url(r'^admin/', admin.site.urls),

    # entity
    url(r'^ajax/addEntity/', BoardView.as_view(action='addEntity'), name='addEntity'),
    url(r'^ajax/getEntityJSON/([0-9]+)/', BoardView.as_view(action='getEntityJSON'), name='getEntityJSON'),
    url(r'^ajax/getEntity/([0-9]+)', BoardView.as_view(action='getEntity'), name='getEntity'),
    url(r'^ajax/updateEntity/', BoardView.as_view(action='updateEntity'), name='updateEntity'),
    url(r'^ajax/addAsEntity/', BoardView.as_view(action='addAsEntity'), name='addAsEntity'),

    # queue
    url(r'^ajax/getGDList/', views.get_google_drive_list, name='getGoogleDriveList'),
    url(r'^ajax/boardNotify/', BoardView.as_view(), name='board_notify'),
    url(r'^ajax/getQueueList/', BoardView.as_view(action='getQueueList'), name='getQueueList'),

    # board
    url(r'^board/(?P<id>[0-9]+)/', BoardView.as_view(), name='board'),
    url(r'^boards/', BoardListView.as_view(), name='boardList'),
    url(r'^ajax/getBoardList/', BoardListView.as_view(action='getBoardList'), name='getBoardList'),
    url(r'^ajax/getNewBoardForm/', BoardListView.as_view(action='getNewBoardForm'), name='getNewBoardForm'),
    url(r'^ajax/getEditBoardForm/([0-9]*)/', BoardListView.as_view(action='getNewBoardForm'), name='getEditBoardForm'),
    url(r'^ajax/deleteBoard/', BoardListView.as_view(action='deleteBoard'), name='deleteBoard'),

    # url for modal window of IMAP acc and filters
    url(r'^ajax/getIMAPFilters/', BoardView.as_view(action='getIMAPFilters'), name='getIMAPFilters'),

    # imap
    url(r'^ajax/addIMAP/', BoardView.as_view(action='addIMAP'), name='addIMAP'),
    url(r'^ajax/editIMAP/([0-9]+)/', BoardView.as_view(action='addIMAP'), name='addIMAPID'),
    url(r'^ajax/deleteIMAP/', BoardView.as_view(action='deleteIMAP'), name='deleteIMAP'),
    url(r'^ajax/testIMAP/', BoardView.as_view(action='testIMAP'), name='testIMAP'),
    # end of imap

    # mail filter
    url(r'^ajax/addFilter/', BoardView.as_view(action='addFilter'), name='addFilter'),
    url(r'^ajax/editFilter/([0-9]+)/', BoardView.as_view(action='addFilter'), name='addFilterID'),
    url(r'^ajax/deleteFilter/', BoardView.as_view(action='deleteFilter'), name='deleteFilter'),
    # END mail filter

    # git webhook
    url(r'^ajax/addWebHook/', BoardView.as_view(action='addWebHook'), name='addWebHook'),
    url(r'^ajax/editWebHook/([0-9]+)/', BoardView.as_view(action='addWebHook'), name='editWebHook'),
    url(r'^ajax/deleteWebHook/', BoardView.as_view(action='deleteWebHook'), name='deleteWebHook'),
    url(r'^ajax/getWebHooks/', BoardView.as_view(action='getWebHooks'), name='getWebHooks'),
    url(r'^webhook/git/(?P<token>[\w-]+)$', GitAPI.as_view()),
    # END git webhook

    # profile
    url(r'^err/', views.err),
    url(r'^ajax/logout/', views.logout_from_google_api),
    url(r'^ajax/trackDirectory/', views.track_directory),
    url(r'^ajax/createEntityFromDrive/', views.create_entity_from_drive),
    url(r'^accounts/register/', RegistrationView.as_view(form_class=MyCustomRegForm)),
    url(r'^profile/pass-changed/', ProfileView.as_view(), name='password_change_done'),
    url(r'^profile/password_change/$', auth_views.password_change, name='password_change'),
    url(r'^profile/', ProfileView.as_view(), name='profile'),
    url(r'^profile/deactivate/', ProfileView.as_view(), name='deactivate'),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^setCode/', views.login_to_google_api),
    # end of profile
]


# debug toolbar
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

urlpatterns += staticfiles_urlpatterns()
