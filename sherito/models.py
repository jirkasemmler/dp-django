from __future__ import unicode_literals

from base64 import b64encode, b64decode
from os import urandom
from Crypto.Cipher import ARC4
from dp import settings
from django.contrib.auth.models import User
from oauth2client.contrib.django_util.models import CredentialsField
import random, string
from django.db import models
import quopri
import email
from Crypto import Random
from Crypto.Cipher import AES
from django.utils.translation import ugettext as _

# encoding: utf-8

class GoogleCredentialsModel(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.OneToOneField(User)
    credential = CredentialsField()


class LocalUser(User):
    user = None

    def __init__(self, user_id):
        self.user = User.objects.get(pk=user_id)

    def update_data(self, data):
        self.user.first_name = data['first_name']
        self.user.last_name = data['last_name']
        self.user.email = data['email']
        try:
            self.user.save()
            return True
        except ValueError:
            return False

    def toggle_activation(self, data):
        action = 'a' if not self.user.is_active else 'd'
        record = ManualActivationChanges(action=action,
                                         reason=data['reason'],
                                         user=self.user)
        record.save()
        self.user.is_active = False if self.user.is_active else True
        self.user.save()


class ManualActivationChanges(models.Model):
    ACTION = (
        ('a', 'activation'),
        ('d', 'deactivation'),
    )

    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now=True)
    action = models.CharField(max_length=1, choices=ACTION)
    reason = models.TextField()
    user = models.ForeignKey(User)


class Dashboard(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    desc = models.TextField()
    gkey = models.TextField()
    user = models.ForeignKey(User)
    active = models.BooleanField(default=True)

    def store_gkey(self, gkey):
        self.gkey = gkey
        self.save()

    def soft_delete(self):
        self.active = False
        self.save()

    def update(self, data):
        self.name = data['name']
        self.desc = data['desc']

        self.save()


class EntitySourceType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def get_icon(self):
        icons = {
            'gdrive': 'gdrive.png',
            'email': 'email.png',
            'git': 'git.png',
            'custom': 'custom.png',
            'url': 'url.png',
        }
        return icons[self.name]

    def __unicode__(self):
        return self.name


class GDriveTrackedFolder(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User)
    fileID = models.CharField(max_length=500, unique=True)
    name = models.CharField(max_length=500)
    board = models.ForeignKey(Dashboard)


def get_value(name):
    def f(self):
        return IMAPCredentials.decrypt(getattr(self, 'e_%s' % name))

    return f


def set_value(name):
    def f(self, value):
        setattr(self, 'e_%s' % name, IMAPCredentials.encrypt(value))

    return f


def get_random_string(length):
    return ''.join(random.choice(string.lowercase) for i in range(length))


class Credentials(models.Model):
    SALT_SIZE = 8

    @staticmethod
    def encrypt(message):
        def pad(s):
            return s + b"\0" * (AES.block_size - len(s) % AES.block_size)

        key = settings.AES_KEY
        message = pad(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(message)

    @staticmethod
    def decrypt(ciphertext):
        key = settings.AES_KEY
        iv = ciphertext[:AES.block_size]
        cipher = AES.new(key, AES.MODE_CBC, iv)
        plaintext = cipher.decrypt(ciphertext[AES.block_size:])
        return plaintext.rstrip(b"\0")

    def setValue(self, name, value):
        setattr(self, name, self.encrypt(value))

    def getValue(self, name):
        self.decrypt(getattr(self, name))


class IMAPCredentials(Credentials):
    name = models.CharField(max_length=128)
    port = models.IntegerField(null=True)
    e_username = models.BinaryField(blank=True)
    e_password = models.BinaryField(blank=True)
    e_host = models.BinaryField(blank=True)
    user = models.ForeignKey(User)

    # @staticmethod
    # todo refactor
    def encrypted_property(name):
        return property(get_value(name), set_value(name))

    username = encrypted_property('username')
    password = encrypted_property('password')
    host = encrypted_property('host')

    def update(self, data):
        self.username = data['username']
        self.password = data['password']
        self.host = data['host']
        self.name = data['name']
        self.save()

    def __unicode__(self):
        return self.name

    def soft_delete(self):
        self.delete()


class EmailFilter(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True)
    board = models.ForeignKey(Dashboard)
    credentials = models.ForeignKey(IMAPCredentials)
    filter_from = models.CharField(max_length=255, blank=True)
    filter_to = models.CharField(max_length=255, blank=True)
    filter_body_kw = models.CharField(max_length=255, blank=True)
    filter_subject_kw = models.CharField(max_length=255, blank=True)

    # last UID of seen msg.
    last_uid = models.IntegerField(null=True)

    def update(self, data, board_id):
        self.name = data['name']
        self.filter_from = data['filter_from']
        self.filter_to = data['filter_to']
        self.filter_body_kw = data['filter_body_kw']
        self.filter_subject_kw = data['filter_subject_kw']
        self.board = Dashboard.objects.get(pk=board_id)
        self.credentials = IMAPCredentials.objects.get(pk=data['credentials'])

        self.save()

    def soft_delete(self):
        self.delete()


class Queue(models.Model):
    id = models.AutoField(primary_key=True)
    source_type = models.ForeignKey(EntitySourceType)
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    waiting = models.BooleanField(default=True)
    active = models.BooleanField(default=True)
    board = models.ForeignKey(Dashboard)

    def use(self, is_active):
        self.waiting = False
        self.active = is_active
        self.save()

    def get_icon(self):
        return self.source_type.get_icon()

    def get_object(self):
        return getattr(self, str(self.source_type.name) + 'itemqueue')


class GDriveItemQueue(Queue):
    gkey = models.CharField(max_length=255, null=True)
    data = models.TextField()
    folder = models.ForeignKey(GDriveTrackedFolder, null=True)


class EmailItemQueue(Queue):
    email_filter = models.ForeignKey(EmailFilter)
    subject = models.CharField(max_length=500)
    body = models.TextField()
    whole_message = models.TextField()
    from_address = models.CharField(max_length=500)
    to_address = models.CharField(max_length=500)

    def get_body(self):
        """
        formatted by encodind and replaced \n to <br> because of displaying in HTML
        :return:
        """
        return quopri.decodestring(self.body).decode('utf-8').replace('\n', '<br />')

    def get_from(self):
        """
        decodes FROM ADDRESS. needs to have extra method because of calling from template
        see self.decode_address
        :return: formated address
        """
        return self.decode_address(self.from_address)

    def get_to(self):
        """
        see get_from
        :return:
        """
        return self.decode_address(self.to_address)

    @staticmethod
    def decode_address(address):
        """
        decodes address from IMAP format to human readable
        IMAP form looks like this : u'=?UTF-8?B?SmnFmcOtIFNlbW1sZXI=?= <jirka.semmler@gmail.com>'
        human readable is 	Jiri (here is ascii) Semmler <jirka.semmler@gmail.com>

        parsing goes by this :
        >> > b = email.header.decode_header(a.from_address)
        >> > b
        [('Ji\xc5\x99\xc3\xad Semmler', 'utf-8'), ('<jirka.semmler@gmail.com>', None)]
        ^^ because
        :param address: address from IMAP
        :return: formatted address
        """

        tmp = email.header.decode_header(address)
        out = ''
        for item in tmp:
            if item[1]:  # there is encoding
                tmp_out = item[0].decode(item[1])
            else:
                tmp_out = item[0]
            out += tmp_out + ' '
        return out


class GitService(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    def get_code(self):
        return self.name.lower()


class GitFilter(models.Model):
    id = models.AutoField(primary_key=True)
    board = models.ForeignKey(Dashboard)
    name = models.CharField(max_length=255)
    token = models.CharField(max_length=255)
    commit_msg_kw = models.CharField(max_length=255)
    service = models.ForeignKey(GitService)

    def update(self, data, board_id):
        self.board = Dashboard.objects.get(pk=board_id)
        self.commit_msg_kw = data['commit_msg_kw']
        self.name = data['name']
        token = get_random_string(32)
        self.token = token
        self.service = GitService.objects.get(id=data['service'])
        self.save()
        return token

    def get_hook_url(self):
        return str(settings.HTTP_PROTOCOL) + str(settings.HTTP_HOST) + "/webhook/git/" + self.token

    def soft_delete(self):
        self.delete()


class GitItemQueue(Queue):
    git_filter = models.ForeignKey(GitFilter)
    message = models.CharField(max_length=500)
    timestamp = models.CharField(max_length=500)
    url = models.CharField(max_length=500)
    user = models.CharField(max_length=500)
    user_email = models.CharField(max_length=500)
    commit_id = models.CharField(max_length=500)

    def get_files(self):
        return GitItemQueueFiles.objects.filter(gititem=self)

    def add_files(self, added, removed, modified):
        self.insert_files(added, 'a')
        self.insert_files(removed, 'r')
        self.insert_files(modified, 'm')

    def insert_files(self, files, action):
        for file_item in files:
            GitItemQueueFiles(filename=file_item,
                              action=action,
                              gititem=self)

    def update(self, data):
        for item in data.keys():
            setattr(self, '%s' % item, data[item])
        self.board = data['git_filter'].board
        self.source_type = EntitySourceType.objects.get(name='git')
        self.name = 'Commit by' + data['user'] + ':' + data['message']
        self.save()


class GitItemQueueFiles(models.Model):
    ACTIONS = (
        ('a', 'Added'),
        ('r', 'Removed'),
        ('m', 'Modified'),
    )

    filename = models.CharField(max_length=500)
    action = models.CharField(max_length=1, choices=ACTIONS)
    gititem = models.ForeignKey(GitItemQueue)


class Entity(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    data = models.TextField(blank=True)
    queue_item = models.ForeignKey(Queue, null=True)
    source_type = models.ForeignKey(EntitySourceType)

    def update(self, data):
        if not self.id:
            if 'queue_id' in data.keys():
                q_item = Queue.objects.get(pk=data['queue_id'])
                self.queue_item = q_item
                self.source_type = q_item.source_type
            else:
                source_type_item = EntitySourceType.objects.get(name=data['source_type'])
                self.source_type = source_type_item
        self.name = data['name']
        if 'data' in data.keys():
            self.data = data['data']
        self.save()
