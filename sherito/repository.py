from models import *

from django.contrib.auth.models import User


class BasicBackend:
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class EmailBackend(BasicBackend):
    def authenticate(self, username=None, password=None):

        try:
            user = User.objects.get(email=username)
            user.username = ''.join(random.choice(string.lowercase) for i in range(8))
            user.save()  # because username is null and it is not unique
        except User.DoesNotExist:
            return None
        if user.check_password(password):
            return user
