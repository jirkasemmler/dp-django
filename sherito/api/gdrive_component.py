from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client.contrib.django_util.storage import DjangoORMStorage
from sherito.models import *


class GDriveComponent(object):
    """
    Class covering complete backend communication with GoogleDriveAPI
    Providing REST communication with Google Drive REST API
    @see https://developers.google.com/api-client-library/python/start/get_started
    """
    flags = None
    user = None
    service = None

    # If modifying these scopes, delete your previously saved credentials
    # at ~/.credentials/drive-python-quickstart.json
    SCOPES = 'https://www.googleapis.com/auth/drive.metadata.readonly'
    # data scope. @see https://developers.google.com/identity/protocols/googlescopes

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # absolute basedir
    CLIENT_SECRET_FILE = os.path.join(BASE_DIR, 'client_secret.json')  # path to GAPI credentials

    APPLICATION_NAME = 'Sherito'

    filter = {}  # templates for filter and query
    query = {"parents": [], "mimeType": '', "trashed": False, 'owners': []}

    # region filterSet
    # <editor-fold desc="filterSet">
    def set_corpora(self, corpora):
        self.filter["corpora"] = corpora

    def set_trashed(self, trashed=False):
        self.query['trashed'] = trashed

    def set_fields(self, file_type='default'):
        if file_type is 'default':
            self.filter["fields"] = "files(id, name, mimeType)"

    def set_mimetype(self, mimetype):
        """
        :param mimetype: 'folder'...
        @see https://developers.google.com/drive/v3/web/mime-types
        """
        data = {
            'folder': 'application/vnd.google-apps.folder'
        }

        if mimetype in data.keys():
            self.query['mimeType'] = data[mimetype]

    def set_owner(self, owner):
        if owner is 'me':
            self.query['owners'] = ['me']

    def set_parents(self, parent):
        self.query['parents'] = [parent]

    def set_query(self):
        """
        creates query string for REST
        :return:
        """
        self.set_trashed()
        query = []  # init query

        for element in self.query.keys():
            if type(self.query[element]) is list and self.query[element]:  #
                query.append("'" + str(self.query[element][0]) + "' in " + element)

            elif type(self.query[element]) is str and self.query[element]:
                query.append(element + " = " + "'" + str(self.query[element]) + "'")

            elif type(self.query[element]) is bool:
                query.append(element + " = " + str(self.query[element]).lower())

        self.filter['q'] = " and ".join(query)

    def create_filter(self):
        """
        creates the filer for query
        """
        self.set_corpora("user")
        self.set_fields()
        # self.set_mimetype("folder")

        self.set_query()
    # </editor-fold>
    # endregion

    def is_logged_in(self, user):
        """

        :param user: models.User
        :return: <service> object if user is logged to GoogleDrive, False is not
        """
        self.user = user
        return self.set_service()

    def set_code(self, auth_code):
        """
        receives the auth_code from GoogleAPI and stores it in the DB
        @see https://developers.google.com/api-client-library/python/guide/django
        :param auth_code: code to store
        :return:
        """
        credentials = client.credentials_from_clientsecrets_and_code(
            self.CLIENT_SECRET_FILE,
            ['https://www.googleapis.com/auth/drive'],
            auth_code)
        store = DjangoORMStorage(GoogleCredentialsModel, 'user_id', self.user, 'credential')  # local storage
        store.put(credentials)  # storing

    def get_credentials(self):
        """
        Gets valid user credentials from storage.
        If nothing has been stored, or if the stored credentials are invalid - returns None
        :return: Credentials, the obtained credential.
        """

        storage = DjangoORMStorage(GoogleCredentialsModel, 'user_id', self.user, 'credential')  # get the storage
        credentials = storage.get()  # get the credentials

        if not credentials or credentials.invalid:
            return None
        else:
            return credentials

    def remove_connection(self):
        """
        revokes the connection. removes the access from users profile
        :return:
        """
        credentials = self.get_credentials()

        if credentials:
            credentials.revoke(httplib2.Http())

    def set_service(self):
        """
        sets credentials and builds the service
        """
        credentials = self.get_credentials()
        if not credentials:
            return False
        http = credentials.authorize(httplib2.Http())
        self.service = discovery.build('drive', 'v3', http=http)

        return True if self.service else False

    def list_dir(self, dir_id='root', set_filter=True):
        """
        lists directory
        :param dir_id: ID of dir to list
        :param set_filter: set filter for storing
        :returns list of files in selected directory by dir_id
        """
        if set_filter:
            self.set_parents(dir_id)
            self.create_filter()
        results = self.service.files().list(**self.filter).execute()

        tracked_files_ids = GDriveTrackedFolder.objects.filter(user=self.user).values_list('fileID', flat=True)
        for file_item in results['files']:
            file_item['tracked'] = file_item['id'] in tracked_files_ids
            file_item['is_folder'] = file_item['mimeType'] == 'application/vnd.google-apps.folder'

        return results.get('files', [])

    def get_parent(self, dir_id):
        """
        returns list of directories which are parents of selected directory
        :param dir_id: selected directory
        :return: file object of directory
        """
        return self.service.files().get(fileId=dir_id, fields="name,parents").execute()

    def get_folder_content(self, dir_dir):
        """
        sets the filters and lists folder
        :param dir_dir: directory google ID
        :return: list of folders
        """
        self.set_corpora("user")
        self.set_fields()
        self.set_parents(dir_dir)

        self.set_query()

        data = self.list_dir(dir_dir, False)
        return data

    def get_file_data(self, file_id):
        """
        get detail data about selected file
        :param file_id:
        :return: file object with data
        """
        fields = 'name,mimeType,description,webContentLink,webViewLink,thumbnailLink,modifiedTime'
        return self.service.files().get(fileId=file_id, fields=fields).execute()

# for local testing
# if __name__ == '__main__':
#  # local testing
#     obj = GAPI()
#     obj.set_service()
#
#     items = obj.list_dir()
#     if not items:
#         print('No files found.')
#     else:
#         print('Files:')
#         for item in items:
#             print(item)
