import logging

import json

from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.renderers import JSONRenderer
from rest_framework.exceptions import ParseError
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from sherito.models import *

from django.http.response import JsonResponse

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.basicConfig()


class GitAPI(GenericAPIView):
    renderer_classes = [JSONRenderer]

    @method_decorator(csrf_exempt)
    def handle_github(self, git_filter, payload):
        """
        parsing github payload
        :param git_filter: models.GitFilter object
        :param payload: received payload, JSON
        :return: boolean
        """
        commits = payload.get('commits')
        for commit in commits:
            msg = commit.get('message')
            kws = git_filter.commit_msg_kw.split(',')
            if not any(kw in msg for kw in kws):
                continue  # there is no interesting keyword (kw) in commit message
            else:
                # parsing all needed data from commit
                user = commit.get('committer').get('name')
                user_email = commit.get('committer').get('email')
                url = commit.get('url')
                timestamp = commit.get('timestamp')
                added = commit.get('added')
                removed = commit.get('removed')
                modified = commit.get('modified')
                commit_id = commit.get('id')

                gititemqueue = GitItemQueue()  # creating item in queue
                gititemqueue.update({'git_filter': git_filter,
                                     'message': msg,
                                     'timestamp': timestamp,
                                     'url': url,
                                     'user': user,
                                     'user_email': user_email,
                                     'commit_id': commit_id})
                gititemqueue.add_files(added, removed, modified)
        return True

    @method_decorator(csrf_exempt)
    def handle_bitbucket(self, git_filter, payload):
        """
        parsing bitbucket payload
        :param git_filter: models.GitFilter object
        :param payload: received payload, JSON
        :return: boolean
        """
        commits = payload.get('push').get('changes')[0].get('commits')
        for commit in commits:
            msg = commit.get('message')
            kws = git_filter.commit_msg_kw.split(',')
            if not any(kw in msg for kw in kws):
                continue  # there is no interesting keyword (kw) in commit message
            else:
                user = commit.get('author').get('user').get('display_name')
                user_email = commit.get('author').get('raw')
                url = commit.get('links').get('self').get('href')
                timestamp = commit.get('date')
                commit_id = commit.get('hash')

                gititemqueue = GitItemQueue()  # creating item in queue
                gititemqueue.update({'git_filter': git_filter,
                                     'message': msg,
                                     'timestamp': timestamp,
                                     'url': url,
                                     'user': user,
                                     'user_email': user_email,
                                     'commit_id': commit_id})
        return True

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        """
        method accepting POST request (via CBV)
        :param request: request object
        :param args:
        :param kwargs:
        :return:
        """
        webhook_token = kwargs.get('token')  # token for identification

        try:
            gitfilter = GitFilter.objects.get(token=webhook_token)
        except GitFilter.DoesNotExist:
            return JsonResponse({'status': 'invalid token'})

        # Git repo information from post-receive payload
        if request.content_type == "application/json":
            payload = request.data
        else:
            # Probably application/x-www-form-urlencoded
            payload = json.loads(request.data.get("payload", "{}"))

        if gitfilter.service.get_code() == 'bitbucket':
            status = self.handle_bitbucket(gitfilter, payload)
        elif gitfilter.service.get_code() == 'github':
            status = self.handle_github(gitfilter, payload)
        else:
            return JsonResponse({'status': 'this service is not implemented yet'})

        if status:
            return JsonResponse({'status': 'accepted'})
