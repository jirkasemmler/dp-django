from __future__ import print_function
import imaplib
from PyMOTW.imaplib.imaplib_list_parse import parse_list_response
from django.utils.translation import ugettext as _

from sherito.models import *


class EmailComponent(object):
    """
    Component providing access to email service via IMAP
    """
    connection = None  # property for storing connection

    def set_connection(self, credentials):
        """
        connects to the IMAP server
        :param credentials: IMAPCredentials
        :return:
        """
        imaplib.IMAP4_SSL_PORT = 993 if credentials.port is None else int(credentials.port)
        # using default of custom port

        self.connection = imaplib.IMAP4_SSL(credentials.host)
        try:
            self.connection.login(credentials.username, credentials.password)
            return self.connection
        except Exception as e:
            print(_("Could not connect to IMAP server - Credentials ID ") + str(credentials.id))
            return False

    # # for local testing
    # def set_local_connection(self):
    #     """
    #     local setting of connection, for testing only
    #     :return:
    #     """
    #     username = ""
    #     password = ""
    #     host = ""
    #
    #     self.connection = imaplib.IMAP4_SSL(host)
    #     self.connection.login(username, password)
    #
    #     return self.connection

    @staticmethod
    def set_searching_query(filter_data):
        """
        creates the searching query from stored filter data
        :param filter_data: models.EmailFilter
        :return: query string
        """
        # all these search parameters work as AND
        ret = ()
        last_uid = '(UID {}:*)'
        subj_str = '(SUBJECT "{}")'
        body_str = '(BODY "{}")'
        from_str = '(FROM "{}")'
        to_str = '(TO "{}")'
        # ^^ strings where the filter will be formatted to

        # todo use days as parameter in filer setting like "check my emails in last X days"
        # todo figure out how to handle first checkout
        if filter_data.last_uid:
            ret += last_uid.format(filter_data.last_uid),
        if filter_data.filter_from:
            ret += from_str.format(filter_data.filter_from),
        if filter_data.filter_to:
            ret += (to_str.format(filter_data.filter_to),)
        if filter_data.filter_body_kw:
            ret += (body_str.format(filter_data.filter_body_kw),)
        if filter_data.filter_subject_kw:
            ret += (subj_str.format(filter_data.filter_subject_kw),)

        return ret

    def check(self):
        """
        goes by all set filters and checks the imap servers - used for CRONjob
        :return:
        """
        filters = EmailFilter.objects.all()
        for email_filter in filters:
            if self.set_connection(email_filter.credentials):
                self.do_fetch(email_filter)

    def do_fetch(self, email_filter):
        """
        Main email communication.
        Expects that the connection is set. Goes by filter, reads emails and stores them to Queue
        :param email_filter: models.EmailFilter object
        :return:
        """
        source = EntitySourceType.objects.get(name='email')
        connection = self.connection
        typ, mail_box_data = connection.list()  # list the mail folders on mail server, LIST command

        # see https://tools.ietf.org/html/rfc3501#section-6.4.4
        query = self.set_searching_query(email_filter)  # gets the searching query
        for line in mail_box_data:
            flags, delimiter, mail_box_name = parse_list_response(line)
            connection.select('"{}"'.format(mail_box_name), readonly=True)  # selects on folder, SELECT command
            typ, msg_ids = connection.search(
                None,
                *query
            )
            print(query)
            """
            how to use the query
            connection.search(
                None,
                # '(SINCE "22-Jul-2016")', # e.g. how to use the query
                # '(BODY "dp")',
                # '(FROM "jirka.semmler@gmail.com")',
                # '(TO "jirka.semmler@gmail.com")',
            )
            """
            uids = []
            for msg in msg_ids[0].split(' '):  # reading messages by UID
                if msg != '':
                    if int(msg) <= email_filter.last_uid:
                        continue
                    msg_typ, msg_data = connection.fetch(msg, '(RFC822)')  # fetching messages, FETCH command
                    mail = email.message_from_string(msg_data[0][1])

                    if mail.is_multipart():
                        body = mail.get_payload()[0].get_payload()
                    else:
                        body = mail.get_payload()

                    email_queue = EmailItemQueue(subject=mail.get('subject'),
                                                 from_address=mail.get('from'),
                                                 to_address=mail.get('to'),
                                                 body=body,
                                                 whole_message=mail.as_string(),
                                                 name=mail.get('subject'),
                                                 source_type=source,
                                                 board=email_filter.board,
                                                 email_filter=email_filter
                                                 )
                    email_queue.save()  # saving email to queue
                    uids.append(int(msg))
            print(uids)
            if uids.__len__() == 0:
                max_uid = 0
            else:
                max_uid = max(uids)
            email_filter.last_uid = email_filter.last_uid if email_filter.last_uid >= max_uid else max_uid
            email_filter.save()

# for console testing
# if __name__ == '__main__':
