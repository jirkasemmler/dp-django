from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from registration.forms import RegistrationForm
from models import *


class BoardForm(forms.ModelForm):
    class Meta:
        model = Dashboard
        fields = ['name', 'desc']


class IMAPCredentialsForm(forms.Form):
    name = forms.CharField(max_length=255, required=True)
    username = forms.CharField(max_length=255, required=True)
    password = forms.CharField(widget=forms.PasswordInput)
    host = forms.CharField(max_length=255, required=True)
    port = forms.IntegerField(required=False)


class MyCustomRegForm(RegistrationForm):
    class Meta(UserCreationForm.Meta):
        fields = [
            'email',
            'password1',
            'password2'
        ]
        required_css_class = 'required'


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class EmailFilterForm(forms.ModelForm):
    class Meta:
        model = EmailFilter
        fields = ['name', 'credentials', 'filter_from', 'filter_to',  'filter_body_kw', 'filter_subject_kw']
        labels = {
            'name': _('Your label'),
            'filter_from': _('Email address FROM'),
            'filter_to': _('Email address TO'),
            'filter_subject_kw': _('Keywords in Subject'),
            'filter_body_kw': _('Keywords in email body'),
        }
        help_texts = {
            'service': _('GitHub or Bitbucker'),
            'filter_subject_kw': _('Divide by comma'),
            'filter_body_kw': _('Divide by comma'),
        }
        error_messages = {

        }


class GitFilterForm(forms.ModelForm):

    class Meta:
        model = GitFilter
        fields = ('name', 'service', 'commit_msg_kw')

        labels = {
            'name': _('Your label'),
            'service': _('Service name'),
            'commit_msg_kw': _('Commit message keywords'),
        }
        help_texts = {
            'service': _('GitHub or Bitbucker'),
            'commit_msg_kw': _('Devide by comma'),
        }
        error_messages = {

        }


class DeactivateForm(forms.Form):
    action = forms.CharField(widget=forms.HiddenInput(), initial='toggle_activation')
    sure = forms.BooleanField()
    reason = forms.CharField(widget=forms.Textarea)
