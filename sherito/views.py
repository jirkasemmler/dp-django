from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.http import HttpResponse, Http404, HttpResponseForbidden, JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.template import loader
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from api.gdrive_component import GDriveComponent
from api.gdrive_mimetypes import MimeType
from forms import *


def get_profile_menu(request):
    """
    common function to generate menu, used in different views
    :param request: http request
    :return: context dict
    """
    context = {}

    if not request.user.is_authenticated:
        profile_items = {
            'icon': "unlogged.png",
            'items': [
                {'url': "/accounts/register/", 'name': 'Sign up'},
                {'url': "/accounts/login/", 'name': 'Login'},
            ]
        }
        items = []
    else:
        profile_items = {
            'icon': "logged.png",
            'items': [
                {'url': "/profile/", 'name': 'Profile'},
                {'url': "/accounts/logout/", 'name': 'Logout'},
            ]
        }
        items = [
            {'url': "/boards/", 'name': 'Boards'},
        ]
    context['menu_items'] = items
    context['profile_items'] = profile_items

    return context


def index(request):
    """
    renders main index page
    :type request: HttpRequest
    """
    template = loader.get_template('index.html')
    context = get_profile_menu(request)

    return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class BoardListView(TemplateView):
    """
    covers all view methods for list of boards
        get
        post
    BoardList shows list of Boards with CRUD
    """
    action = 'default'  # default action. Action is parameter which determines what to do in all the GET/POST requests
    template_name = "boardList/main.html"
    context = {}
    request = None

    def get_context(self, local_context):
        """
        method for generating context for rendering. Merges local (from views) context and
        common context (menu)
        :param local_context: dict with local context
        :return: dict with context
        """
        context = get_profile_menu(self.request)
        context.update(local_context)
        return context

    def get(self, request, *args):
        """
        method for handling all GET requests for this CBV
        :param request: http request
        :param args:
        :return:
        """
        self.request = request  # storing request

        # determining what to do by the action. see urls.py, the action is set there
        if self.action == 'getBoardList':  # getting list of boards via Ajax
            self.get_board_list(request)  # sets the template and context

        elif self.action == 'getNewBoardForm':  # getting form for creating new board
            board_id = None if args.__len__() == 0 else args[0]  # edit or create
            self.get_form(board_id)

        return render(request, self.template_name, self.get_context(self.context))  # http response

    def get_board_list(self, request):
        """
        generating view with list of boards for selected user. Used for Ajax
        :param request: http request needed for user
        :return: void
        """
        self.template_name = 'boardList/list_applet.html'
        self.context = {
            'boards': Dashboard.objects.filter(user=request.user, active=True),  # sending data to template
        }

    def get_form(self, board_id=None):
        """
        sets context with form for create/edit board
        :param board_id: ID of board to edit. None if creating
        :return: void
        """
        self.template_name = 'boardList/form_applet.html'  # setting template
        if board_id:  # edit
            board = Dashboard.objects.get(pk=board_id)  # get the existing board
            form = BoardForm(instance=board)  # building form with existing instance
            self.context = {
                'form': form,
                'edit_id': board_id
            }

        else:  # create
            form = BoardForm()  # creating new board
            self.context = {
                'form': form
            }

    def get_form_posted(self, request):
        """
        handling POST request with filled form
        :param request: httpd request
        :return: void
        """
        form = BoardForm(request.POST)  # filling form with sended data
        if form.is_valid():
            if 'id' in request.GET.keys():  # edit
                board = Dashboard.objects.get(pk=request.GET['id'])
                board.update(request.POST)
                request._messages.add(messages.INFO, _("board edited"))
            else:  # create
                board = Dashboard(name=request.POST['name'],  # create the object
                                  desc=request.POST['desc'],
                                  active=True,
                                  user=request.user)
                board.save()
                request._messages.add(messages.INFO, _("board created"))
            self.get(request)  # answesing with get method

        else:  # form is not valid, returning failed form
            self.template_name = 'boardList/form_applet.html'
            self.context = {
                'form': form
            }

    @staticmethod
    def delete_board(request):
        """
        deleting board via ajax
        :param request: http request
        :return: boolean
        """
        board_id = request.POST['id']
        board = get_object_or_404(Dashboard, pk=board_id)  # searching board to delete
        try:
            board.soft_delete()  # soft delete of the board
            return True
        except ValueError:
            return False

    def post(self, request, *args):
        """
        handling all POST requests, determining by self.action parameter from urls.py
        :param request: http request
        :param args:
        :return:
        """
        self.request = request
        if self.action == "default":  # default action, posted board form
            self.get_form_posted(request)
        elif self.action == "deleteBoard":  # deleting board
            status = "ok" if self.delete_board(request) else "err"
            return JsonResponse({'status': status})  # answer by JSON

        # answer by view
        return render(request, self.template_name, self.get_context(self.context))


@method_decorator(login_required, name='dispatch')
class ProfileView(TemplateView):
    template_name = "app/profile.html"  # default template
    request = None

    def get_context(self, local_context):
        """
        see BoardListView.get_context()
        :param local_context:
        :return:
        """
        context = get_profile_menu(self.request)
        context.update(local_context)
        return context

    def get(self, request, *args):
        """
        all GET requests
        :param request: http request
        :param args: params from ?key=value
        :return: http response
        """
        self.request = request
        gdrive_component = GDriveComponent()
        is_logged = gdrive_component.is_logged_in(request.user)

        user = get_object_or_404(User, id=request.user.id)
        # setting all forms to template, because all of them are available in one page
        edit_profile_form = EditProfileForm(instance=user)
        pass_form = PasswordChangeForm(user)
        deactivate_form = DeactivateForm()

        context = {
            'is_logged': is_logged,
            'editProfileForm': edit_profile_form,
            'passForm': pass_form,
            'deactivateForm': deactivate_form,
            'user': request.user
        }

        # rendering http response with template and context
        return render(request, self.template_name, self.get_context(context))

    def post(self, request):
        """
        handling all POST requests
        :param request: http request
        :return: responsing via GET method
        """
        self.request = request

        user = LocalUser(request.user.id)

        # action is set in POST
        if 'action' in request.POST.keys() and request.POST['action'] == 'toggle_activation':
            form = DeactivateForm(request.POST)
            if form.is_valid() and request.POST['sure']:  # form validation
                user.toggle_activation(request.POST)  # storing data
                msg = _('Account activated') if user.user.is_active else _('Account deactivated')  # translated message
                request._messages.add(messages.INFO, msg)
            return self.get(request)  # answering via GET

        else:
            form = EditProfileForm(request.POST)  # checking form
            if form.is_valid():
                if user.update_data(request.POST):
                    request._messages.add(messages.SUCCESS, _("profile edited"))
                else:
                    request._messages.add(messages.WARNING, _("profile edit failed"))
                return HttpResponseRedirect('/profile/')  # answer via redirect
            else:
                return self.get(request)  # answering with GET


@method_decorator(login_required, name='dispatch')
class BoardView(TemplateView):
    """
    renders single board, main view. the rest is JavaScript magic
    """
    template_name = "board/board.html"
    action = "default"
    board_id = None
    context = {}

    @staticmethod
    def get_context(local_context):
        """
        see BoardListView.get_context()
        :param local_context:
        :return:
        """
        context = {
            'menu_items': [
                {'url': "/boards/", 'name': _('Boards')},
                {'url': "/accounts/logout/", 'name': _('Logout')},
                {'url': "/profile/", 'name': _("Profile")}

            ]
        }
        context.update(local_context)
        return context

    def get_imap_credentials_form_posted(self, request):
        """
        handling posted form for imap credentials
        :param request:
        :return:
        """
        form = IMAPCredentialsForm(request.POST)
        if form.is_valid():  # form validation
            if 'id' in request.GET.keys():  # create / edit
                imap_object = IMAPCredentials.objects.get(pk=request.GET['id'])  # getting credentials form
                imap_object.update(request.POST)  # storing data
                request._messages.add(messages.INFO, _("IMAPCredentials changed"))
            else:
                imap_object = IMAPCredentials(name=request.POST['name'], user=request.user)  # creating credentials form
                imap_object.update(request.POST)  # storing data
                request._messages.add(messages.INFO, _("IMAPCredentials created"))
            self.context = {}
            self.get(request)  # answering with get method, flash messages are set

        else:  # form is NOT valid
            self.template_name = 'board/emails_imap-credentials_form.html'
            self.context = {
                'form': form
            }

    def get_email_filter_form_posted(self, request):
        """
        storing data for form with email filter.
        the same principe as previous, see self.get_imap_credentials_form_posted
        :param request: http request
        :return: void
        """
        form = EmailFilterForm(request.POST)
        if form.is_valid():
            if 'id' in request.GET.keys():
                filter_obj = EmailFilter.objects.get(pk=request.GET['id'])
                filter_obj.update(request.POST, request.session['board_id'])
                request._messages.add(messages.INFO, _("imap filter changed"))
            else:
                filter_obj = EmailFilter()
                filter_obj.update(request.POST, request.session['board_id'])
                request._messages.add(messages.INFO, _("imap filter created"))
            self.context = {}
            self.get(request)

        else:
            self.template_name = 'board/emails_imap-credentials_form.html'
            self.context = {
                'form': form
            }

    def get_webhook_form_posted(self, request):
        """
        handling posted form for setting webhook
        the same principe as previous, see self.get_imap_credentials_form_posted
        :param request:
        :return:
        """
        form = GitFilterForm(request.POST)
        if form.is_valid():
            if 'id' in request.GET.keys():
                git_filter_obj = GitFilter.objects.get(pk=request.GET['id'])
                git_filter_obj.update(request.POST, request.session['board_id'])
                request._messages.add(messages.INFO,
                                      _('webhook changed, your url is :  %(url)s') % {
                                          'url': git_filter_obj.get_hook_url()}
                                      )
                # defining the URL for webhook. available in detail as well but distributed in flash message
            else:
                filter_obj = GitFilter()
                filter_obj.update(request.POST, request.session['board_id'])
                request._messages.add(messages.INFO,
                                      _('webhook created, your url is :  %(url)s') % {
                                          'url': filter_obj.get_hook_url()})
            self.context = {}
            self.get(request)

        else:
            self.template_name = 'board/githook_form.html'
            self.context = {
                'form': form
            }

    # @staticmethod
    # def build_hook_url(request, token):
    # probably deprecated. todo remove
    #     return str(request.scheme) + "://" + str(request.META['HTTP_HOST']) + "/webhook/git/" + token

    def post(self, request, *args, **kwargs):
        """
        handling all POST requests associated with board
        :param request: http request with POST data
        :param args: args from url
        :param kwargs:
        :return:
        """
        req_type = None if 'type' not in request.POST.keys() else request.POST['type']

        board_id = request.session['board_id']  # board id is stored in session from first GET request to board

        board = Dashboard.objects.get(pk=board_id)
        if board.user != request.user:  # checking access
            return HttpResponseForbidden

        if req_type == "change_gkey":  # storing gkey, when the board is connected to google drive
            gkey = request.POST['gkey']
            status = board.store_gkey(gkey)

        elif self.action == 'default':  # determining by action parameter as usual
            self.board_id = board_id
            # these actions are the same, and must be determined by content in POST
            # todo refactoring candidate

            if 'username' in request.POST.keys():  # imap credentials
                self.get_imap_credentials_form_posted(request)

            elif 'filter_from' in request.POST.keys():  # imap filter
                self.get_email_filter_form_posted(request)

            elif 'commit_msg_kw' in request.POST.keys():  # webhook
                self.get_webhook_form_posted(request)

            # rendering http response with template
            return render(request, self.template_name, self.get_context(self.context))

        elif self.action == "deleteFilter":  # deleting imap filter
            status = "ok" if self.delete_filter(request) else "err"
            return JsonResponse({'status': status})

        elif self.action == "getEntityJSON":  # get details about entity
            entity_id = None if args.__len__() == 0 else args[0]
            entity = Entity.objects.get(pk=entity_id)
            return JsonResponse({'name': entity.name, 'data': entity.data})  # json response for Ajax

        elif self.action == "addAsEntity":
            queue_item = Queue.objects.get(pk=request.POST['id'])
            queue_item.use(request.POST['action'] == "add")
            entity = Entity()
            entity.update({'name': queue_item.name,
                           'queue_id': queue_item.id,
                           })
            status = "ok"

            return JsonResponse({'status': status, 'entity_id': entity.id})

        elif self.action == "deleteWebHook":
            status = "ok" if self.delete_webhook(request) else "err"
            return JsonResponse({'status': status})

        elif self.action == "deleteIMAP":
            status = "ok" if self.delete_IMAP(request) else "err"
            return JsonResponse({'status': status})

        elif self.action == "updateEntity":
            status = "ok" if self.add_entity(request) else "err"
            return JsonResponse({'status': status})

        elif self.action == "addEntity":
            entity = self.add_entity(request)
            return JsonResponse({'status': 'ok', 'entity_id': entity.id, 'type': 'custom', 'name': entity.name})

        elif self.action == "testIMAP":  # testing imap connection (username/pass)
            try:
                credentials_id = request.POST['imap_id']

                credentials = IMAPCredentials.objects.get(pk=credentials_id)
                from sherito.api.email_component import EmailComponent  # custom import
                mailapi = EmailComponent()
                # testing by setting connection. if the connection is set, the credentials are correct
                status = "ok" if mailapi.set_connection(credentials) else "err"
            except Exception as e:
                print e
                status = 'not-found'
            return JsonResponse({'status': status})  # json response for Ajax

        else:
            status = "err"  # unknown action
        return JsonResponse({'status': status})

    @staticmethod
    def add_entity(request):
        """
        adding entity to DB
        :param request: http request
        :return:
        """
        entity_id = None if 'entity_id' not in request.POST.keys() else request.POST['entity_id']  # edit or create
        if entity_id:  # edit
            entity = Entity.objects.get(pk=entity_id)
        else:  # create
            entity = Entity()

        try:
            entity.update(request.POST)  # storing sent data
            return entity
        except Exception as e:
            print e  # printing err, available in log afterwords
            return False

    @staticmethod
    def delete_webhook(request):
        """
        deleting webhook for git
        :param request:
        :return:
        """
        webhook_id = request.POST['id']
        webhook = get_object_or_404(GitFilter, pk=webhook_id)
        try:
            webhook.soft_delete()  # using soft delete
            return True
        except Exception as e:
            print e  # printing err, available in log afterwords
            return False

    def delete_IMAP(self, request):
        imap_acc_id = request.POST['id']
        imap_acc = get_object_or_404(IMAPCredentials, pk=imap_acc_id)
        try:
            imap_acc.soft_delete()
            return True
        except Exception as e:
            print e  # printing err, available in log afterwords
            return False

    @staticmethod
    def delete_filter(request):
        """
        deleting email imap filter
        :param request:
        :return:
        """
        filter_id = request.POST['id']
        filter_item = get_object_or_404(EmailFilter, pk=filter_id)  # getting EmailFilter object
        # if not found -> 404 response
        try:
            filter_item.soft_delete()  # soft delete
            return True
        except Exception as e:
            print e  # printing err, available in log afterwords
            return False

    def get(self, request, *args, **kwargs):
        """
        handling all GET requests for board, determining by self.action parameter
        mainly for Ajax communication
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        if self.action == "default":  # default, rendering page
            board_id = self.board_id if self.board_id else kwargs['id']
            request.session['board_id'] = board_id
            board = get_object_or_404(Dashboard, pk=board_id)  # searching board
            gdrive_component = GDriveComponent()
            is_logged = gdrive_component.is_logged_in(request.user)

            context = {'board_id': board_id,
                       'unlogged': not is_logged,
                       'board': board
                       }
            return render(request, self.template_name, self.get_context(context))  # rendering page

        elif self.action == "getQueueList":  # list with queue
            self.template_name = 'app/queue_applet.html'  # using template for response
            self.get_queue(request.session['board_id'])

        elif self.action == "addIMAP":  # asking for form
            imap_id = None if args.__len__() == 0 else args[0]  # create / edit
            self.get_imap_form(request, imap_id)  # getting form

        elif self.action == "addFilter":
            filter_id = None if args.__len__() == 0 else args[0]
            self.get_filter_form(request, filter_id)

        elif self.action == "addWebHook":
            hook_id = None if args.__len__() == 0 else args[0]
            self.get_webhook_form(request, hook_id)

        elif self.action == "getWebHooks":
            self.get_webhooks(request.session['board_id'])

        elif self.action == "getIMAPFilters":
            self.get_imap_filter_list(request.session['board_id'])

        elif self.action == "getEntity":  # get entity detail
            entity_id = None if args.__len__() == 0 else args[0]
            self.get_entity_detail(request, entity_id)

        # rendering http response based on previous actions. handling these actions is bellow
        return render(request, self.template_name, self.get_context(self.context))

    def get_entity_detail(self, request, entity_id):
        """
        get detail about entity in HTML
        :param request:
        :param entity_id:
        :return:
        """
        entity = Entity.objects.get(pk=entity_id)
        # switching response by entity type, because each type needs custom view
        if entity.source_type.name == 'url':
            self.template_name = 'board/entity_detail/url.html'
            self.context = {
                'entity': entity,
            }
        if entity.source_type.name == 'email':
            self.template_name = 'board/entity_detail/email.html'
            self.context = {
                'entity': entity,
                'email': entity.queue_item.get_object(),  # email message
                # get_object() return object with with entity which inherits from queue
            }
        elif entity.source_type.name == 'git':
            self.template_name = 'board/entity_detail/git.html'
            self.context = {
                'entity': entity,
                'commit': entity.queue_item.get_object(),  # commit
            }
        elif entity.source_type.name == 'gdrive':
            self.template_name = 'board/entity_detail/gdrive.html'
            gdrive_component = GDriveComponent()  # initializing GoogleComponent object which works with GDrive API
            gdrive_component.user = request.user
            gdrive_component.set_service()  # setting service to be able to get the data from GDrive
            gkey = entity.queue_item.get_object().gkey

            gdata = gdrive_component.get_file_data(gkey)  # get detail data about GDrive file

            # translation of google mimeType

            mimetype = MimeType()
            self.context = {
                'entity': entity,
                'gdata': gdata,
                'mimetype': mimetype.get_name_of_mimetype(gdata['mimeType'])
            }
        else:  # custom
            self.template_name = 'board/entity_detail/custom.html'
            self.context = {
                'entity': entity,
            }

    def get_webhooks(self, board_id):
        """
        get webhooks available for board
        :param board_id:
        :return:
        """
        self.template_name = 'board/githook_applet.html'  # using custom template

        board = Dashboard.objects.get(pk=board_id)
        hooks = GitFilter.objects.filter(board=board)  # get hooks

        self.context = {
            'githooks': hooks,  # sending data to template
        }

    def get_imap_filter_list(self, board_id):
        """
        getting details about email filters and imap accounts
        :param board_id:
        :return:
        """
        self.template_name = 'board/emails_applet.html'

        board = Dashboard.objects.get(pk=board_id)
        filters = EmailFilter.objects.filter(board=board)  # rendering filters and accounts in one window together
        accounts = IMAPCredentials.objects.filter(user=board.user)

        self.context = {
            'filters': filters,
            'accounts': accounts,
        }

    def get_imap_form(self, request, imap_id=None):
        """
        form for imap account
        :param request:
        :param imap_id: id if edit
        :return:
        """
        self.template_name = 'board/emails_imap-credentials_form.html'
        if imap_id:  # edit
            imap_account = IMAPCredentials.objects.get(pk=imap_id)
            form = IMAPCredentialsForm(initial={'name': imap_account.name,
                                                'username': imap_account.username,
                                                'password': imap_account.password,
                                                'host': imap_account.host,
                                                'port': imap_account.port,
                                                })  # imap form is not DRY mapped to model, custom initial data needed
            self.context = {
                'form': form,
                'edit_id': imap_id,
                'board_id': request.session['board_id']
            }
        else:
            form = IMAPCredentialsForm()
            self.context = {
                'form': form,
                'board_id': request.session['board_id']
            }

    def get_webhook_form(self, request, webhook_id=None):
        """
        requesting for form to create/edit git webhook
        :param request:
        :param webhook_id:
        :return:
        """
        self.template_name = 'board/githook_form.html'
        if webhook_id:  # edit
            webhook = GitFilter.objects.get(pk=webhook_id)
            form = GitFilterForm(instance=webhook)

            self.context = {
                'form': form,
                'edit_id': webhook_id,
                'board_id': request.session['board_id']
            }
        else:
            form = GitFilterForm()
            self.context = {
                'form': form,
                'board_id': request.session['board_id']
            }

    def get_filter_form(self, request, filter_id=None, *args, **kwargs):
        """
        form for email filter for imap
        :param request:
        :param filter_id:
        :param args:
        :param kwargs:
        :return:
        """
        self.template_name = 'board/emails_filter_form.html'
        if filter_id:  # edit
            email_filter = EmailFilter.objects.get(pk=filter_id)
            form = EmailFilterForm(instance=email_filter)
            self.context = {
                'form': form,
                'edit_id': filter_id,
                'board_id': request.session['board_id']
            }
        else:  # create
            form = EmailFilterForm()
            self.context = {
                'form': form,
                'board_id': request.session['board_id']
            }

    def get_queue(self, board_id):
        """
        returns queue for selected board
        :param board_id:
        :return:
        """
        # get only active and waiting items from queue
        items_in_queue = Queue.objects.filter(board_id=board_id, active=True, waiting=True)
        self.context = {
            'data': items_in_queue,
            'counter': items_in_queue.__len__(),
            'show_last': items_in_queue.__len__() == 0
        }


def logged(view):
    """
    wrapper for decorator
    :param view:
    :return:
    """

    def wrapper(request, *args):
        if not request.user.is_authenticated:
            messages.add_message(request, messages.INFO, _("you must log first"))
            return HttpResponseRedirect('/accounts/login')
        else:
            return view(request, *args)

    return wrapper


def err(request):
    """
    renders err page
    :param request:
    :return:
    """
    template = loader.get_template('err.html')
    context = {
    }

    return HttpResponse(template.render(context, request))


def track_directory(request):
    """
    FBV, storing data about folder from GoogleDrive to be able to check it in cronjob afterwords
    track and untrack operations, JSON response for Ajax request
    :param request:
    :return:
    """
    action = request.POST['action']
    user = request.user
    file_gkey = request.POST['file_gkey']
    board = Dashboard.objects.get(pk=request.session['board_id'])
    source_type = EntitySourceType.objects.get(name='gdrive')
    entity_id = None

    if action == 'track':
        try:
            GDriveTrackedFolder.objects.get(user=user, fileID=file_gkey, board=board)
            status = 'alreadyExists'
        except GDriveTrackedFolder.DoesNotExist:
            item = GDriveTrackedFolder(user=user, fileID=file_gkey, board=board)
            item.save()
            status = 'saved'
        try:
            item_queue = GDriveItemQueue.objects.get(gkey=file_gkey)
        except GDriveItemQueue.DoesNotExist:
            item_queue = GDriveItemQueue(gkey=file_gkey,
                                         board=board,
                                         active=True,
                                         waiting=False,
                                         name=request.POST['name'],
                                         source_type=source_type
                                         )
            item_queue.save()

        entity = Entity(name=request.POST['name'], queue_item=item_queue, source_type=source_type)
        entity.save()

        entity_id = entity.id
    else:
        try:
            item = GDriveTrackedFolder.objects.get(user=user, fileID=file_gkey)
            item.delete()
            status = 'deleted'

        except GDriveTrackedFolder.DoesNotExist:
            status = 'not found'

    return JsonResponse({'status': status, 'entity': entity_id})


def get_google_drive_list(request):
    """
    :returns HTML of applet.html with list of available files/dirs from selected dir from GDrive
    gets list of elements from GDrive and renders them into applet.html
    :param request: HttpRequest
    :return: HttpResponse
    """
    gdrive_component = GDriveComponent()  # initializing GoogleComponent object which works with GDrive API
    get_ar = request.POST
    gdrive_component.user = request.user
    gdrive_component.set_service()

    if gdrive_component.is_logged_in(request.user):
        root = get_ar["id"]
        data = gdrive_component.list_dir(root)
        print(data)
        print(data.__len__())
        parent_obj = gdrive_component.get_parent(root)  # parent dir is important for going back

        parent = {'name': parent_obj['name'],
                  'id': "" if 'parents' not in parent_obj.keys() else parent_obj['parents'][0]}

        if not request.is_ajax():
            raise Http404
        else:
            template = loader.get_template('google_drive_applet.html')
            context = {
                'elements': data,  # list of files
                'parent': parent,  # parent dir
                'empty': data.__len__() == 0,
                'logged': True
            }
    else:
        template = loader.get_template('google_drive_applet.html')
        context = {
            'logged': False
        }
    return HttpResponse(template.render(context, request))


def create_entity_from_drive(request):
    """
    creates entity from google drive list of files. it is not from the queue but from the list
    :param request:
    :return:
    """
    file_gkey = request.POST['file_gkey']
    board = Dashboard.objects.get(pk=request.session['board_id'])
    source_type = EntitySourceType.objects.get(name='gdrive')


    try:
        item_queue = GDriveItemQueue.objects.get(gkey=file_gkey)
    except GDriveItemQueue.DoesNotExist:
        item_queue = GDriveItemQueue(gkey=file_gkey,
                                     board=board,
                                     active=True,
                                     waiting=False,
                                     name=request.POST['name'],
                                     source_type=source_type
                                     )
        item_queue.save()

    entity = Entity(name=request.POST['name'], queue_item=item_queue, source_type=source_type)
    entity.save()

    entity_id = entity.id

    return JsonResponse({'status': 'ok', 'entity_id': entity_id})


def logout_from_google_api(request):
    """
    removes credentials to GoogleComponent and removes this app from users supported apps
    :param request:
    :return:
    """
    gdrive_component = GDriveComponent()
    gdrive_component.user = request.user

    if gdrive_component.remove_connection():
        status = 'ok'
    else:
        status = 'err'
    return JsonResponse({'status': status})


def login_to_google_api(request):
    """
    stores the GoogleComponent credentials to DB and sets the key with GoogleComponent
    :param request:
    :return:
    """
    auth_code = request.POST['code']
    gdrive_component = GDriveComponent()
    gdrive_component.user = request.user

    gdrive_component.set_code(auth_code)
    return JsonResponse({'status': 'ok'})
