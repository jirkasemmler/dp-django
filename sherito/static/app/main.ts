/**
 * Created by stet on 15.4.17.
 */

//<editor-fold desc="oauth_back">

/**
 * sending login code to DB. used after the document is loaded
 * @param authResult
 */
function signInCallback(authResult) {
    if (authResult['code']) {
        $.ajax({
            type: 'POST',
            url: '/setCode/',
            success: function (result) {
            },
            data: {'code': authResult['code']}
        });
    }
    else {
        // There was an error.
        alert("Oops! Something went wrong.");
    }
}

/**
 * handler for login button
 */
$('#signinButton').click(function () {
    startOauth();
    // auth2 is included from CDN
    auth2.grantOfflineAccess().then(signInCallback);  // using Promises
});
//</editor-fold>

/**
 * loading board lists
 */
function initBoardList()
{
    ajaxHandler('GET', "list", {}, boardListSuccess, {'target': '#list'})
}

/**
 * generating new form for new board
 * @param obj
 */
function getNewBoardForm(obj)
{
    let id = $(obj).attr('id');
    ajaxHandler('GET', id, {}, ajaxHTMLSuccess, {'target': $(obj).data("targethtml")})
}

/**
 * deleting board
 * @param id
 */
function deleteBoard(id)
{
    let element = $("#"+id);
    ajaxHandler('POST', id,
        {'id': element.data('boardid')},
        deleteBoardSuccess,
        {'target': 'boardRow'+element.data('boardid')}
        );
}

/**
 * removing board from DOM
 * @param data
 * @param target
 */
function deleteBoardSuccess(data, target)
{
    if(data.status == 'ok')
    {
        $("#"+target.target).hide('slow');
    }
    else
    {
        alert("Oops! Something went wrong.");
    }
}

/**
 * renders boards in list. inits the confirmation
 * @param data
 * @param target
 */
function boardListSuccess(data, target)
{
    $(target.target).html(data);

    $('[data-toggle=confirmation]').confirmation({ // confirm
        rootSelector: '[data-toggle=confirmation]',
    });
}


/**
 * logout from Google services
 * @param objId
 */
function logout(objId) {
    ajaxHandler('POST', objId, {}, logoutSuccess);
}

/**
 * letting user know that he has been logged out
 * @param data
 */
function logoutSuccess(data) {
    alert("You have been logged out");
}