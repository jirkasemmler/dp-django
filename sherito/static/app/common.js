/**
 * Created by stet on 29.4.17.
 */
/**
 * providing login to Google Oauth
 */
function startOauth() {
    gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
            client_id: '916951550395-ls3u7icq84okuj8st6kl3ssccr1c3kmh.apps.googleusercontent.com',
            // Scopes to request in addition to 'profile' and 'email'
            scope: 'https://www.googleapis.com/auth/drive'
        });
    });
}
//<editor-fold desc="csrf">
/*
 following code and functions provides the CSRF token for forms.
 see https://docs.djangoproject.com/en/1.11/ref/csrf/
 */
function getCookie(name) {
    let cookieValue = null;
    let i = 0;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (i; i < cookies.length; i++) {
            let cookie = $.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
let csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    crossDomain: false,
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
// no operation. used as empty handler
function nop() {
}
//</editor-fold>
/**
 * AJAX handler used in all the AJAX communication. sends the AJAX requests to the server and calls callbacks afterwords
 * @param method - POST/GET method
 * @param elementID  - element that contains data about the request
 * @param data  - object with data to send to the server
 * @param responseFun  - pointer to function/method called after successful (200) response
 * @param afterResponseData  - data to pass to responseFun
 */
function ajaxHandler(method, elementID, data, responseFun, afterResponseData = undefined) {
    let element = $("#" + elementID);
    let url = element.data("url"); // element MUST contain URL where send the AJAX request to
    $.ajax({
        type: method,
        url: url,
        data: data,
        success: (responseData) => {
            if (responseData == undefined) {
                responseFun(responseData);
            }
            else {
                responseFun(responseData, afterResponseData);
            }
        }
    });
}
/**
 * universal functions. gets the HTML content from the server and pass it in the opened modal
 * @param obj - html object with data
 */
function universalModal(obj) {
    let id = $(obj).attr('id');
    ajaxHandler('GET', id, {}, ajaxHTMLSuccess, { 'target': $(obj).data("targethtml") });
}
/**
 * fills the target element with HTML data
 * @param data - HTML
 * @param target - target element
 */
function ajaxHTMLSuccess(data, target) {
    $(target.target).html(data);
}
/**
 * AJAX handler used for deleting. sends the delete operation to the server and removes the HTML element from DOM
 * @param objectID
 */
function deleteItem(objectID) {
    let obj = $("#" + objectID);
    ajaxHandler('POST', objectID, { 'id': obj.data('itemid') }, deleteItemSuccess, { 'target': obj.data('target') });
}
/**
 * callback for deleteItem().
 * @param data
 * @param target
 */
function deleteItemSuccess(data, target) {
    if (data.status == 'ok') {
        $("#" + target.target).hide('slow');
    }
    else {
        alert("Oops! Something went wrong.");
    }
}
/**
 * init function for bootstrap modal widget. see vendor/bootstrap-confirmation2
 */
function initConfirm() {
    $("[data-toggle='modal']").click(function () {
        $(".modal-forms").hide();
        let action = $(this).data("action");
        $("#" + action).show();
    });
}
/**
 * swapping key and value in object
 * @param json
 * @return {{}}
 */
function swap(json) {
    let ret = {};
    for (let key in json) {
        ret[json[key]] = key;
    }
    return ret;
}
//# sourceMappingURL=common.js.map