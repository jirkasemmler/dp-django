/**
 * Created by stet on 11.3.17.
 */

declare let utils: any;
declare let $: any;
declare let SVG: any;


declare let gapi: any;
type Rect = svgjs.Rect;
type SVGElement = svgjs.Element;
type Group = svgjs.G;
type Doc = svgjs.Doc;
type SvgXml = string;

class Colors {

	colors = {
		'red': '#DB2828',
		'orange': '#F2711C',
		'yellow': '#FBBD08',
		'olive': '#B5CC18',
		'green': '#21BA45',
		'teal': '#00B5AD',
		'blue': '#2185D0',
		'violet': '#6435C9',
		'purple': '#A333C8',
		'pink': '#E03997',
	};
	colorCodes = [];
	colorPointer: number = 0;

	constructor() {
		this.colorCodes = Object.keys(swap(this.colors));
	}

	decodeColor(colorKey) {

		let objToSearchIn = (colorKey[0] == "#") ? swap(this.colors) : this.colors;

		return objToSearchIn[colorKey];
	}

	getColor(): string {
		let pointer = (this.colorPointer++) % this.colorCodes.length;
		return this.colorCodes[pointer]
	}

	setTempColor(color): void {
		this.colorPointer = Object.keys(this.colors).indexOf(color);
	}

}

/**
 * class for entity communication with local database. Each entity has record in DB
 */
class LocalApp {

	/**
	 creates entity in DB
	 @param: data = {name, color, position, type, edata: { gkey }
     @return: entityID
     */
	createEntity(data) {
		$.ajax({
			type: 'POST',
			url: '/ajax/addEntity/',
			data: {'name': data.entity_name, 'data': data.data, 'source_type': data.source_type},
			//
			success: (responseData) => {
				create(responseData.name, responseData.type, responseData.entity_id);
			}
		});
	}

	/**
	 * updates entity in DB
	 * @param data
	 * @return void
	 */
	static updateEntity(data) {
		/*
		 difference from create is that now the entity exists
		 and I don't need to return entity_id and update source_type
		 */
		$.ajax({
			type: 'POST',
			url: '/ajax/addEntity/',
			data: {
				'entity_id': data.entity_id,
				'name': data.entity_name,
				'data': data.data,
			},
			success: (responseData) => {

			}
		});
	}

	/**
	 * @param id
	 */
	deleteEntity(id) {

	}

	getPromisedEntity(id) {
		return new Promise(
			function (resolve, reject) {
				$.ajax({
					type: 'POST',
					url: '/ajax/getEntityJSON/' + id + '/',
					data: {},
					success: (responseData) => {
						resolve(responseData)
					}
				});
			}
		)
	}


	/**
	 * returns data for detail. Using Promises
	 * @param id
	 */
	getEntity(id) {
		this.getPromisedEntity(id).then((result) => {
			initEditForm(result.name, result.data);  // preparing form for edit
		}).catch(() => {
				alert("Oops! Something went wrong.");
			}
		);
	}
}

class AppGapi {
	clientId: string = '916951550395-ls3u7icq84okuj8st6kl3ssccr1c3kmh.apps.googleusercontent.com';
	initRect: any;
	initModel: any;
	realtimeUtils: any;
	model: any;  // GAPI model
	doc: any;
	svgModel: SVGModel; // reference to SVGModel, to be able to manipulate with SVG

	datasetSelected: string;
	iterationsModel: any;
	localmodel: LocalApp;

	constructor(svgModel) {

		// initializing init data
		this.svgModel = svgModel;
		this.svgModel.gapi = this;
		this.localmodel = new LocalApp();

		// init realtimeutils for GoogleRealtimeAPI communication. needed the cleint ID
		// see https://developers.google.com/google-apps/realtime/overview
		this.realtimeUtils = new utils.RealtimeUtils({clientId: this.clientId});
		this.authorize();
	}

	/**
	 * starts the Google Drive communication.
	 * sets the GKey to DB
	 * calls onFileLoaded and onFileInitialized (for following actions)
	 * @return {boolean}
	 */
	start() {
		if (!document.getElementById("drawing")) {  // this code was not called on page with app
			return false;
		}
		// With auth taken care of, load a file, or create one if there is not an id in the URL.
		let id = this.realtimeUtils.getParam('id');
		if (id) { // Load the document id from the URL
			// calling onFL / onFI
			this.realtimeUtils.load(id.replace('/', ''), onFL, onFI);
		}
		else { // Create a new document, add it to the URL
			// TODO name of file
			this.realtimeUtils.createRealtimeFile('new file', (createResponse) => {
				window.history.pushState(null, null, '?id=' + createResponse.id);  //pushing Gkey to URL
				ajaxHandler('POST', 'drawing', {  // inserting URL to DB
					gkey: createResponse.id,
					type: 'change_gkey',
					board_id: $("#drawing").data("board_id")
				}, nop); // nop() = there is no needed action after
				this.realtimeUtils.load(createResponse.id, onFL, onFI);
			});
		}
	}

	/**
	 * creates new iteration in model and in local
	 */
	createNewIteration() {
		let index = parseInt(this.iterationsModel.get(this.iterationsModel.length - 1).replace("data", ""));
		// get index of iteration
		let newIterationName = "data" + String(index + 1);  // set name of iteration
		this.datasetSelected = newIterationName;
		let localModel = this.doc.getModel(); //

		let newMap = localModel.createMap(); // creates model structure
		this.copyElements(newMap); // copy selected elements to new iteration
		this.iterationsModel.push(newIterationName);  // appending name to list

		localModel.getRoot().set(newIterationName, newMap);  // setting new iteration model to gapi model

		this.model = localModel.getRoot().get(this.datasetSelected); // setting new created model as current model

		this.initEvents(); // attaching events to new model
		this.loadElements();  // loading elements
		this.renderIterations();  // render side panel with iterations
	}

	/**
	 * chaning iteration current -> selected
	 * @param selectedIteration - name of selected iteration
	 */
	changeIteration(selectedIteration) {
		this.datasetSelected = selectedIteration;
		this.model = this.doc.getModel().getRoot().get(this.datasetSelected);
		// setting iteration's model as current model

		// see this.createNewIteration()
		this.initEvents();
		this.loadElements();
		this.renderIterations();
	}

	/**
	 * rendering panel with iterations
	 */
	renderIterations() {
		let iterations = this.iterationsModel.asArray();
		let ul = $('#iterationsSelector');
		ul.html('');  // cleaning target container
		let i = 1;
		for (let j = 0; j < iterations.length; j++)  // iterating over all iterations
		{
			let element = iterations[j];
			// building HTML
			let iterationMarkup = $('<li class="list-group-item"><a href="javascript:changeIteration(\'' + element + '\')">Itetation ' + i + '</a>');
			if (element == this.datasetSelected) {
				iterationMarkup.addClass("list-group-item-info").addClass("active-iteration");
			}
			i++;
			ul.append(iterationMarkup);  // appending html
		}
	}


	/**
	 * authorizes the client to GDrive, opens the auth window if needed
	 */
	authorize() {
		// Attempt to authorize
		this.realtimeUtils.authorize((response) => {
			if (response.error) {
				// Authorization failed because this is the first time the user has used your application,
				// show the authorize button to prompt them to authorize manually.
				$("#gdrive-integrate_dialog").modal("show");
				let button = $("#auth_button");  // shows the button where the user can authorize the access to GDrive
				button.bind('click', () => {
					this.realtimeUtils.authorize((response) => {
						this.start(); // starting communication
					}, true);
				});
			}
			else {
				this.start(); // starting communication
			}
		}, false);
	}

	/**
	 * creates the whole model. it is called when the document didnt exist before
	 *
	 * @param model - model of GDrive realtime document - internal representation of board data
	 * model is CollaborativeMap ( see https://developers.google.com/google-apps/realtime/reference/gapi.drive.realtime.CollaborativeMap)
	 * where each SVG node has its own record by its ID
	 */
	onFileInitialize(model) {
		let colMap = model.createMap(); // creates model structure
		let colMapRect = model.createMap(); // creates model structure
		let colIterationsList = model.createList(); // creates model structure

		this.svgModel.setInitRect();
		let initData = this.svgModel.getInitData(); // gets init data

		// fills the structure with data
		model.getRoot().set('iterations', colIterationsList);
		model.getRoot().get('iterations').push('data1');
		model.getRoot().set('data1', colMap);
		model.getRoot().set('initRect', colMapRect);
		model.getRoot().get('initRect').set('svg', initData.svg);
		model.getRoot().get('initRect').set('style', initData.style);
	}


	/**
	 * registration for event callbacks
	 */
	initEvents() {
		/** for each event needed it defines a callback */
		this.model.addEventListener(
			gapi.drive.realtime.EventType.VALUE_CHANGED,
			eventHandlerOut);
		this.initModel.addEventListener(
			gapi.drive.realtime.EventType.VALUE_CHANGED,
			eventHandlerRectOut);
	}


	/**
	 * event handler for changing background style
	 * @param event
	 * @return {boolean}
	 */
	static eventHandlerRect(event) {
		if (event.property == 'style' && !event.isLocal) {
			changeRectBackground(event.newValue);
			return true;
		}
	}

	/**
	 * events handlers
	 * @param event - structure from Google RealtimeAPI with everything about the event
	 */
	eventHandler(event) {
		/*
		 event structure
		 ...
		 newValue : Object
		 data : Object
		 groupID : "SvgjsG1021"
		 id : "SvgjsText1019"
		 msg : "moved"
		 pos : Object
		 type : "text"
		 oldValue : Object
		 data : Object
		 groupID : "SvgjsG1021"
		 id : "SvgjsText1019"
		 msg : "moved"
		 pos : Object
		 type : "text"
		 ...
		 property : "SvgjsText1019"
		 ...
		 */
		if (event.newValue == undefined) // delete action come without newValue property
		{
			this.svgModel.deleteElement(event.oldValue.id);
			return;
		}
		switch (event.newValue.msg) { // event type is determined by the msg property
			case "new": {  // new entity created
				if (!event.isLocal) {
					this.svgModel.addShape(event.newValue.data, event.newValue.id, event.newValue.type, event.newValue.groupID);
				}
			}
				break;
			case "moved": {  // entity moved
				if (!event.isLocal) {
					this.svgModel.moveElement(event.newValue.id, event.newValue.pos);
				}
			}
				break;
			case "edited": { // entity edited
				if (!event.isLocal) {
					this.svgModel.editElement(event.newValue);
				}
			}
				break;

			default:
				alert("Oops! Something went wrong. I don't recognize the incoming event.");
		}
	}

	/**
	 * file exists, it just needed to be filled with GDrive data
	 * called even for new document
	 * @param doc - data coming from GDrive
	 */
	onFileLoaded(doc) {
		/** loads data in local structure */
		this.doc = doc;

		this.iterationsModel = doc.getModel().getRoot().get("iterations");

		this.datasetSelected = this.iterationsModel.get((this.iterationsModel.length) - 1);

		this.model = doc.getModel().getRoot().get(this.datasetSelected);
		this.initModel = doc.getModel().getRoot().get("initRect");
		let initRect = this.initModel.get('svg');
		let initStyle = this.initModel.get('style');
		this.svgModel.setRect(initRect, initStyle);

		// rendering iteration, see this.createNewIteration()
		this.initEvents();
		this.loadElements();
		this.renderIterations()
	}

	/**
	 * coppying checked elements to new iteration model (newMap)
	 * @param newMap - collaborative model for new iteration, by reference
	 */
	copyElements(newMap) {
		let groups_to_copy = [];

		let checked = $(".checked-entity");  // current entities from HTML
		for (let i = 0; i < checked.length; i++) {
			let selectedEntity = checked[i];
			groups_to_copy.push(selectedEntity.parentElement.parentElement.parentElement.id); // jumping to parent group
			// pushing elements to copy
		}

		let current_data = this.model.values();  // current entities from collaborative model
		current_data.forEach(function (element) {

			// if it is in selected entities, copy it
			if (groups_to_copy.indexOf(element.groupID) >= 0) // copy this element
			{
				newMap.set(element.id, element);
			}
		});
	}

	/**
	 * rendering elements for new model
	 */
	loadElements() {
		this.svgModel.draw.clear(); // removing all elements from board
		/** gets main data and renders them*/
		let values = this.model.values();
		// for each item create a shape in local SVG client
		// if new document, values are empty
		for (let item in values) {
			this.svgModel.addShape(values[item].data,
				values[item].id,
				values[item].type,
				values[item].groupID,
				values[item]);
		}
		AppGapi.removeLoading();
	}

	static removeLoading() {
		$("#loading").hide();
	}

	/**
	 * createing new entity in model
	 *  @param data - object with attributes : text,
	 */
	setNewElementToModel(data) {
		let elements = this.svgModel.newShape(data); //create element's structure to model

		for (let item in elements) { // that's why python devs love type script
			this.model.set(elements[item].id, elements[item]);
		}
	}

	/**
	 * changing background of canvas in collaborative model
	 * @param typeBack
	 */
	changeInitRectModel(typeBack) {
		this.initModel.set('style', typeBack);
	}


	/**
	 * handler for LOCAL DELETE event
	 * @param id - ID of entity
	 */
	sendDeletingAction(id) {
		this.model.delete(id);
	}

	/**
	 * handler for LOCAL DRAG event
	 */
	sendMovingAction(transformed, id, type, groupID) {
		let element = this.model.get(id); // element in group

		let tmpStructure = {
			id: id,
			pos: transformed,
			msg: "moved",
			type: type,
			groupID: groupID,
			data: element.data
		};
		this.model.set(id, tmpStructure);
	}

	/**
	 * entity was updated and needs to be updated in GDrive file as well
	 * entity contains different SVG elements (circle, text...)
	 * @param data - data about all elements in entity
	 */
	sendEditingAction(data) {
		let textData = data['text'];
		let circleData = data['circle'];

		let textElement = this.model.get(textData.id); // element in group

		// textElement.data.text = textData.text;
		let tmpStructure = {
			id: textData.id,
			pos: textElement.pos,
			msg: "edited",
			type: textElement.type,
			groupID: textData.groupID,
			data: {text: textData.text}
		};
		this.model.set(textElement.id, tmpStructure);

		// vv circle update
		let circleElement = this.model.get(circleData.id); // element in group

		let tmp = {
			radius: circleElement.data.radius,
			fill: circleElement.data.fill,
			type: circleElement.data.type,
			entity_id: circleElement.data.entity_id,
			stroke: circleData.stroke
		};
		// a new tmp is needed because I can not update existing object
		let tmpStructure2 = {
			id: circleData.id,
			pos: circleElement.pos,
			msg: "edited",
			type: circleElement.type,
			groupID: circleData.groupID,
			data: tmp
		};

		this.model.set(circleData.id, tmpStructure2);
	}
}

class SVGModel {

	draw: Doc;
	rect: Rect;
	rectColor: string;
	gapi: AppGapi;
	colors: Colors;

	constructor() {
		this.draw = SVG('drawing').size(1200, 800);
		this.rectColor = "transparent";
		this.colors = new Colors();
	}

	editElement(data) {
		let element = this.getElement(data.id);

		if (data.type == 'circle') {
			element.stroke(data.data.stroke);
		}
		if (data.type == 'text') {
			element.text(data.data.text);
		}
	}

	setInitRect() {
		this.rect = this.draw.rect(1200, 800);
		this.rect.fill(this.rectColor);
	}

	setRect(svg: SvgXml, style: string) {
		this.draw.svg(svg);
		changeRectBackground(style);
	}

	getInitData() {
		return {svg: this.rect.svg(), style: 'grid'};
	}

	/**
	 * returns SVGjs object (not just DOM as jQuery)
	 * @param id
	 * @return SVGElement
	 */
	getElement(id):svgjs.Element {
		return this.draw.select("#" + id).get(0);  // svg always returns array
	}


	/**
	 * moving by REMOTE client
	 * @param id
	 * @param newPos
	 */
	moveElement(id: string, newPos: any) {
		let element = this.getElement(id);
		element.parent().animate({duration: 500}).transform(newPos);
	}

	/**
	 * deleting by REMOTE client
	 * @param id
	 */
	deleteElement(id: string) {
		let element = this.getElement(id);
		if (element && element.parent()) {
			element.parent().remove();
		}
	}

	/**
	 * updating entity by local event
	 * @param data
	 */
	updateEntity(data) {

		let group_element = this.getElement(data.group_id);
		let circle_element = group_element.children()[0];
		circle_element.stroke({color: data.color});
		let text_element = group_element.children()[2];
		text_element.text(data.entity_name);

		let sendData = {
			'circle': {
				id: circle_element.id(),
				msg: "edited",
				type: "circle",
				groupID: group_element.id,
				stroke: data.color

			},
			'text': {
				id: text_element.id(),
				msg: "edited",
				type: "text",
				groupID: group_element.id,
				text: data.entity_name
			},
		};
		this.gapi.sendEditingAction(sendData);  // sending change to GAPI
	}

	/**
	 * getting group for following usage. returns existing if exists
	 * @param id
	 * @return Group
	 */
	getGroup(id = undefined): Group {
		let group = this.draw.group();

		if (id != undefined) {  // does the group with selected ID exist?
			group.node.id = id;
		}

		group.id = group.node.id;
		// setting drag handler. after end of dragging, the dragHandler is called
		group.on("dragend", this.dragHandler, {group: group, self: this});
		group.draggable(); // setting this group able to drag

		return group;
	}


	/**
	 * factory for creating objects - circle | text
	 * @param type
	 * @param data
	 * @return {any}
	 */
	getUniElement(type, data) {
		let ret;
		switch (type) {
			case "circle": {
				ret = this.draw.circle(data.radius).attr({fill: data.fill}).stroke({
					color: data.stroke,
					opacity: 0.6,
					width: 5
				});
				break;
			}
			case "text": {
				ret = this.draw.text(data.text).cy(50).cx(50);
				break;
			}
			default:
				ret = undefined;
		}
		return ret;
	}

	/**
	 * displaying dialog for edit
	 */
	editDialogHandler() {
		this.self.gapi.localmodel.getEntity(this.entity_id);
		$("#entity_id").val(this.entity_id);
		$("#group_id").val(this.group.id);
		$("#form-loading").show();

		$("#" + this.self.colors.decodeColor(this.group.children()[0]._stroke)).prop('checked', true); // stroke color
		$("#saveEditEntityBtn").show();
		$("#createEntityBtn").hide();
		$('#editModal').modal('show');
	}

	/**
	 * setting icon for deteail
	 * @param entity_type
	 * @return {string}
	 */
	getEntityTypeIcon(entity_type) {
		// static url is defined in HTML
		return '<img src="' + static_url + 'imgs/' + entity_type + '.png" class="icon_image" />';
	}

	/**
	 * elements used for extra features like edit / del / view
	 * @param rad - radius/size of circle where it pinned to
	 * @param parentGroup
	 * @param entityType
	 * @param entityID
	 * @returns Group that contain all of these elements (bottons)
	 */
	getEditElement(rad, parentGroup, entityType, entityID) {
		let group = this.draw.group(); // group where add all these elements to

		// edit btn
		let editPosX = 75; // positioning on the circle
		let editPosY = -15;
		let fobjEdit = this.draw.foreignObject(50, 50).x(editPosX).y(editPosY); //foreign object can contain HTML
		fobjEdit.attr({id: 'fobj' + group.id()});  // setting ID
		let edithtml = '<button class="btn btn-info btn-edit"><i class="fa fa-pencil"></i></button>';
		$('#' + 'fobj' + group.id()).html(edithtml);  // passing HTML to foreign object
		// setting action after click
		fobjEdit.on("click", this.editDialogHandler, {group: parentGroup, self: this, entity_id: entityID});

		// del btn
		let delPosX = (rad) * 1.2;
		let delPosY = (rad / 2);
		let fobjDel = this.draw.foreignObject(50, 50).cx(delPosX).cy(delPosY);
		fobjDel.attr({id: 'fobjDel' + group.id()});
		let delHTML = '<button class="btn btn-danger btn-edit"><i class="fa fa-trash"></i></button>';
		$('#' + 'fobjDel' + group.id()).html(delHTML);
		fobjDel.on("click", this.removeHandler, {group: parentGroup, self: this});

		let viewPosX = 30;
		let viewPosY = -30;
		let fobjView = this.draw.foreignObject(50, 50).x(viewPosX).y(viewPosY);
		fobjView.attr({id: 'fobjView' + group.id()});
		let ViewHTML = '<button class="btn btn-default btn-edit btn-entity_type" ' +
			'onclick="showDetail(this)" ' +
			'data-url="/ajax/getEntity/' + entityID + '" ' +
			'data-target="#previewModal" ' +
			'id="preview' + entityID + '" ' +
			'data-toggle="modal" ' +
			'data-targethtml="#previewModalContent" ' +
			'data-entityid="' + entityID + '">' +
			this.getEntityTypeIcon(entityType)
			+ '</button>';
		$('#' + 'fobjView' + group.id()).html(ViewHTML);

		let checkPosX = (rad / 2) * 1.26;
		let checkPosY = rad;
		let fobjcheck = this.draw.foreignObject(50, 50).cx(checkPosX).cy(checkPosY);
		fobjcheck.attr({id: 'fobjcheck' + group.id()});
		let checkHTML = '<button class="btn-check" ' +
			'onclick="checkEntity(this)" ' +
			'data-entityid="' + entityID + '">' +
			'<i class="fa fa-circle-o"></i>'
			+ '</button>';
		$('#' + 'fobjcheck' + group.id()).html(checkHTML);

		// adding objects to the group
		group.add(fobjEdit);
		group.add(fobjDel);
		group.add(fobjView);
		group.add(fobjcheck);

		$(group.node).addClass("editElement");
		return group
	}

	/**
	 * creates Element from GAPI
	 * @param data
	 * @param id
	 * @param type
	 * @param groupID
	 * @param value
	 */
	addShape(data: any, id: string, type: string, groupID: string, value?: any) {
		let group = this.getElement(groupID); // get a group from factory
		let element = this.getUniElement(type, data); // get an element from factory

		element.node.id = id;

		if (!group) // exists already
		{
			group = this.getGroup(groupID);
		}
		group.addArrange(element); // rearranging z-index

		if (type == "circle") {
			group.data("circle", id);
			let editElement = this.getEditElement(data.radius, group, data.type, data.entity_id);
			editElement.ignore = true;  // setting as local element, so it doesn't count as collaborative children
			group.addArrange(editElement);
		}

		$(group.node).addClass("group"); // for better addressing by CSS selector

		if (value.pos) {
			group.transform(value.pos);  // transforming position (if added by GAPI
		}
	}

	/**
	 * create new circ
	 * @return {any|Animation|void}
	 */
	newCirc() {
		return this.draw.circle(100).attr({
			cx: 50,
			cy: 50,
			fill: "#fafafb",
		}).stroke({color: this.colors.getColor(), opacity: 0.6, width: 5});
		// ..
	}

	/**
	 * creating new text element with name
	 * @param name
	 * @return {any}
	 */
	newText(name) {
		let text = this.draw.text(name);
		text.cy(50).cx(50);

		return text;
	}

	/**
	 * creates Element locally
	 * @param data
	 * @returns [{circle}, {text}]  see return array
	 */
	newShape(data) {

		let circ = this.newCirc();
		let text = this.newText(data.text);

		let group = this.getGroup();

		// ok, I have circ, text, group, now I need the extra SVG elements for actions (edit...)
		let editElement = this.getEditElement(100, group, data.type, data.entity_id);
		editElement.ignore = true;  // not collaborative

		group.addArrange(circ);
		group.addArrange(text);
		group.addArrange(editElement);
		$(group.node).addClass("group");

		// extensible for extra nodes
		return [{
			id: circ.id(),
			pos: null,
			type: "circle",
			msg: "new",
			// svg: circ.svg(),
			groupID: group.id,
			data: {
				radius: circ.attr("r") * 2, fill: circ.attr("fill"), stroke: circ.attr('stroke'),
				type: data.type,
				entity_id: data.entity_id
			}
		},
			{
				id: text.id(),
				pos: null,
				type: "text",
				msg: "new",
				// svg: text.svg(),
				groupID: group.id,
				data: {
					text: text.text()
				}
			},
		];
	}

	/**
	 * moving handler for LOCAL event
	 */
	dragHandler() {
		/*
		"this" is goes from structure from newShape
		{
			group: group, - group what has been moved with
			self: this - this object (svgModel)
		}
		cookie test - did you read it? You are awesome!
		send me an email at jirka.semmler@gmail.com, I've a cookie for you
		 */
		let transformed = this.group.transform();
		let group: Group = this.group;
		let childs = group.childrenModel();
		for (let child in childs) {
			// distributing moving actions to collaborative childs
			this.self.gapi.sendMovingAction(transformed, childs[child].id(), childs[child].type, group.id);
		}
	}

	/**
	 * deleting handler for LOCAL event. same idea as dragHandler()
	 */
	removeHandler() {

		let group: Group = this.group;
		let childs = group.childrenModel();
		for (let child in childs) {
			this.self.gapi.sendDeletingAction(childs[child].id());
		}
		this.group.remove();
	}

	/**
	 * changing background of canvas
	 * @param typeBack
	 */
	changeBack(typeBack) {
		changeRectBackground(typeBack);
		this.gapi.changeInitRectModel(typeBack);
	}
}


//<editor-fold desc="app init">
class App {
	gapiobj: AppGapi;
	svgModel: SVGModel;

	constructor() {
		this.svgModel = new SVGModel();
		this.gapiobj = new AppGapi(this.svgModel);
	}
}

SVG.Parent.prototype.addArrange = function (element, i) {
	this.add(element, i);
	let childs = this.children();
	for (let child in childs) {
		if (childs[child].type == "text") {
			childs[child].forward();
		}
	}
};
SVG.Parent.prototype.childrenModel = function () {
	let tmp = this.children();
	let out = [];

	for (let item in tmp) {
		if (!tmp[item].ignore) {  // this element counts as children to collaborate
			out.push(tmp[item]);
		}
	}

	return out;
};

let app = new App();
//</editor-fold>

//<editor-fold desc="app handlers">

// GUI handlers calling the app. refactoring candidate
function onFI(model) {
	app.gapiobj.onFileInitialize(model);
}

function onFL(doc) {
	app.gapiobj.onFileLoaded(doc);
}

function create(name, type, entity_id) {
	app.gapiobj.setNewElementToModel({text: name, type: type, entity_id: entity_id});
}

function eventHandlerOut(event) {
	app.gapiobj.eventHandler(event);
}

function eventHandlerRectOut(event) {
	app.gapiobj.eventHandlerRect(event);
}
// </editor-fold>

//<editor-fold desc="ui handlers">

/*
handlers for loading GoogleDrive Data from DB/backend via ajax
 */

/**
 * displays list of folders in modal to choose from
 * @param obj
 * @param id
 */
function getGDData(objThis, id) {
	let obj = $("#" + id);
	// displaying the loading bar
	$(".loading").show();
	let height = $(".loading").parent().height();
	$(".loading").css("height", height);
	$($(obj).data("targethtml")).hide("slide", {direction: "left"}, 500); // positioning of loading bar

	// get and handle data
	ajaxHandler('POST', $(obj).attr("id"), {'id': $(obj).data("directory")}, getGDDataSuccess, {'target': $(obj).data("targethtml")});
}

// data loaded, render them
function getGDDataSuccess(data, target) {
	$(target.target).html(data);  // data comes in HTML and formated
	$(target.target).show("slide", {direction: "right"}, 500);
	$(".loading").hide();
}

function err() {
	alert("Oops! Something went wrong.");
}

/**
 * handler for marking GoogleDrive directory as "to un/track"
 * @param obj - scope
 * @param id - ID of directory
 */
function trackDirectory(objThis, id) {
	let obj = $("#" + id);
	let name = $(obj).data("name");
	let data = {
		'action': $(obj).hasClass('tracked') ? "untrack" : "track",
		'file_gkey': $(obj).data("file_gkey"),
		'name': name
	};
	// handling ajaxRequest
	ajaxHandler('POST', id, data, changeTrackState, {'target': "#" + id, 'action': data.action, 'name': name})
}

/**
 * changing vizualization of selected folder
 * @param data
 * @param target
 */
function changeTrackState(data, target) {
	let addClass, removeClass, eye;

	if (target.action == 'track') {
		addClass = "tracked";
		removeClass = "untracked";
		eye = "fa-eye-slash";
	}
	else {
		addClass = "untracked";
		removeClass = "tracked";
		eye = "fa-eye";
	}
	$(target.target).addClass(addClass);
	$(target.target).removeClass(removeClass);
	$(target.target).html('<i class="fa ' + eye + '"></i>');

	if (target.action == 'track')
	{
		create(target.name, 'gdrive', data.entity_id)
	}
}

function createEntityFromDrive(objThis, id)
{
	let obj = $("#" + id);
	let name = $(obj).data("name");
	let data = {
		'file_gkey': $(obj).data("file_gkey"),
		'name': name
	};
	ajaxHandler('POST', id, data, addAsEntitySuccess, {'target': "#li" + $(obj).data("file_gkey"), 'dont_recount': true, 'name': name, 'type':'gdrive'})
}

/**
 * loading list with queue
 */
function loadQueue() {
	ajaxHandler('GET', 'queue-list', {}, getGDDataSuccess, {'target': '#queue-list'});
}

/**
 * handler for Queue - if click on "add"
 * @param obj
 */
function addAsEntity(obj) {
	let id = $(obj).attr('id');
	ajaxHandler('POST', id, {  // storing to DB,
		'id': $(obj).data('queueitemid'),
		'action': 'add',
		"type": "removeFromQueue"
	}, addAsEntitySuccess, // creating in SVG and collaborative model
		{'name': $(obj).data("name"), 'type': $(obj).data("type"), 'target': $(obj).data("target")});
}

/**
 * callback from addAsEntity. Creates entity (create()) in SVG and collaborative model
 * @param data
 * @param target
 */
function addAsEntitySuccess(data, target) {
	create(target.name, target.type, data.entity_id);
	$(target.target).hide("slide", {direction: "right"}, 800);
	if(!target.dont_recount)
	{
		changeCounter(); // changing counter in Queue
	}
}

/**
 * user clicked on "remove from queu". just marking in DB and removing from visualization
 * @param obj
 */
function removeFromQueue(obj) {
	let id = $(obj).attr('id');
	ajaxHandler('POST', id, {
		'id': $(obj).data('queueitemid'),
		'action': 'remove',
		"type": "removeFromQueue"
	}, removeFromQueueSuccess, {'target': $(obj).data("target")});
}

/**
 * ajax handler in removeFromQueue
 * @param data
 * @param target
 */
function removeFromQueueSuccess(data, target) {
	$(target.target).hide("slide", {direction: "left"}, 800);
	changeCounter();
}

/**
 * changing Queue counter
 */
function changeCounter() {
	let obj = $('#counter-span');
	let newValue = obj.data('value') - 1;
	obj.html(newValue);
	obj.data('value', newValue);

	if (newValue == 0) {
		$('.last-in-queue').show('slow');  // nothing in queue
	}
}


$(document).ready(function () {
	loadQueue();

	$('body').on('paste', function (e) {
		e.preventDefault();  // handling pasting from clipboard
		let text = (e.originalEvent || e).clipboardData.getData('text/plain');
		handleClipboard(text);
	});
});

/**
 * creating entity based on ctrl+v from clipboard. shows creation from with predefined data
 * @param text
 */
function handleClipboard(text) {

	resetEditForm();

	$("#saveEditEntityBtn").hide();
	$("#createEntityBtn").show();
	$("#editModal").modal('show');

	$("#entity_desc").val(text);
}

/**
 * hiding form, universal
 * @param obj_id
 */
function hideForm(obj_id) {
	$('#' + obj_id).empty();
}

/**
 *
 * @param target_id
 */
function showData(target_id) {
	$('#' + target_id).toggle('slow');
}

/**
 * testing IMAP connection
 * @param obj
 * @param imapID - IMAP DB ID
 */
function testIMAP(obj, imapID) {
	let objID = $(obj).attr('id');
	ajaxHandler('POST', objID, {'imap_id': imapID}, testIMAPSuccess, {})

}

/**
 * callback for imap handler, see testIMAP()
 * @param data
 */
function testIMAPSuccess(data) {
	if (data.status == 'ok') {
		alert("Yahoo, it works :)")
	}
	else {
		alert("ERROR - the connection could not be established.")
	}
}

/**
 * ajax handler for saving entity from form. can not be just posted because the entity needs to be created
 */
function saveEntity() {
	let data = {
		'entity_id': $('#entity_id').val(),
		'group_id': $('#group_id').val(),
		'entity_name': $('#entity_name').val(),
		'entity_desc': $('#entity_desc').val(),
		'color': app.svgModel.colors.decodeColor($('input[name=color]:checked').val())
	};
	LocalApp.updateEntity(data); // update to DB, async
	app.svgModel.updateEntity(data);  // update to Google API
	$('#editModal').modal('hide');  // close the dialog
}

/**
 * detail of entity
 * @param obj
 */
function showDetail(obj) {
	ajaxHandler('GET', $(obj).attr("id"), {}, showDetailSuccess, {'target': $(obj).data("targethtml")});
}

/**
 * rendering the entity data in form
 * @param data - data from GET
 * @param target - target where to render in
 */
function showDetailSuccess(data, target) {
	$(target.target).html(data);
}

/**
 * reseting form for next usage
 */
function resetEditForm() {
	$('#group_id').val('');
	$('#entity_name').val('');
	$('#entity_desc').val('');
	// $('input[name=color]').prop('checked', false);
}

/**
 * rendering new empty form for creating of entity
 */
function renderNewEntityForm() {
	resetEditForm(); // reset all values to default

	$("#saveEditEntityBtn").hide(); // it is create, not edit
	$("#createEntityBtn").show();
	$("#editModal").modal('show'); // displaying the modal
}

/**
 * render form for editing entity
 * @param name
 * @param desc
 */
function initEditForm(name, desc) {
	$("#form-loading").hide();
	$("#entity_name").val(name);
	$("#entity_desc").val(desc);
}

/**
 * handling all "create entity" events from GUI
 */
function createEntity() {

	app.svgModel.colors.setTempColor($('input[name=color]:checked').val());
	app.gapiobj.localmodel.createEntity({ // get data and send it to the app!
			'entity_name': $('#entity_name').val(),
			'data': $('#entity_desc').val(),
			'source_type': 'custom',
		}
	);

	$("#saveEditEntityBtn").show();
	$("#createEntityBtn").hide();
	$('#editModal').modal('hide');

}


/**
 * handler for changing background, sending to app
 * @param typeBack
 */
function changeInitRectClick(bg) {
	app.svgModel.changeBack(bg);
}

/**
 * changes the background in GUI
 * @param bg
 */
function changeRectBackground(bg) {
	$("#drawing").attr('class', bg);
}

/**
 * handler for new iteration, sending to app
 */
function newIteration() {
	app.gapiobj.createNewIteration();
}

/**
 * handler for change iteration, sending to app
 */
function changeIteration(selected) {
	app.gapiobj.changeIteration(selected);
}

/**
 * checking entity, GUI only
 */
function checkEntity(obj) {
	$(obj).children().toggleClass("fa-circle-o");
	$(obj).children().toggleClass("fa-check-circle-o");
	$(obj).toggleClass("checked-entity");
}

//<editor-fold desc="oauth_back">

/**
 * sending login code to DB. used after the document is loaded
 * @param authResult
 */
function signInCallback(authResult) {
    if (authResult['code']) {
        $.ajax({
            type: 'POST',
            url: '/setCode/',
            success: function (result) {
            },
            data: {'code': authResult['code']}
        });
    }
    else {
        // There was an error.
        alert("Oops! Something went wrong.");
    }
}

/**
 * handler for login button
 */
function makeSomeNoice()
{
    startOauth();
    // auth2 is included from CDN
    auth2.grantOfflineAccess().then(signInCallback);  // using Promises
}
//</editor-fold>