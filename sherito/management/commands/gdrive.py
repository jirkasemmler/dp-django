from django.core.management.base import BaseCommand
from sherito.api.gdrive_component import *


class Command(BaseCommand):
    """
    command line handler for checking new files in tracked folders on GoogleDrive
    """
    def handle(self, *args, **options):
        """
        main handler
        :param args:  arguments from cmd line
        :param options:
        :return:
        """
        gapi = GDriveComponent()
        gdrive_source = EntitySourceType.objects.get(name='gdrive')
        credentials_list = GoogleCredentialsModel.objects.all()  # get all reasonable credentials to GDrive
        # todo ^^ active users only
        for credentials_item in credentials_list:
            gapi.user = credentials_item.user_id # set user
            gapi.set_service()  # set service by the credential

            # get all the tracked folders
            tracked_folders = GDriveTrackedFolder.objects.filter(user=credentials_item.user_id)
            for folder in tracked_folders:
                list_from_gdrive = gapi.get_folder_content(folder.fileID)  # get list of files from GDrive

                # get gkeys only about the files in queue
                tracked_files = GDriveItemQueue.objects.filter(board=folder.board,
                                                               source_type=gdrive_source).values_list('gkey',
                                                                                                      flat=True)
                # find new files
                new_files = self.dict_diff(list_from_gdrive, tracked_files)

                for new_file in new_files:  # save the new files/folders
                    q = GDriveItemQueue(source_type=gdrive_source,
                                        name=new_file['name'],
                                        gkey=new_file['id'],
                                        board=folder.board
                                        )
                    q.save()

    @staticmethod
    def dict_diff(list_from_gdrive, current_files):
        """
        finds differences between two dicts
        :param list_from_gdrive:  - bigger
        :param current_files:  - smaller
        :return:
        """
        out = {}  # output dict
        for key_item in list_from_gdrive:
            out[key_item['id']] = key_item['name']
        keys = out.keys()
        needed_keys = list(set(keys) - set(current_files))  # differences by two same structures

        return [{'id': needed_key, 'name': out[needed_key]} for needed_key in needed_keys]  # output structure
