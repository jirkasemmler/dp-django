from django.core.management.base import BaseCommand
from sherito.api.gdrive_component import *


class Command(BaseCommand):
    """
    sets the init data for testing
    """
    help = 'run "python manage.py init" to set init data'

    def handle(self, *args, **options):
        """
        tmp data and init static data
        :param args:
        :param options:
        :return:
        """

        # needed static data
        a = EntitySourceType(name="gdrive")
        a.save()
        aa = EntitySourceType(name="email")
        aa.save()
        aaa = EntitySourceType(name="git")
        aaa.save()
        aaaa = EntitySourceType(name="custom")
        aaaa.save()
        aaaaa = EntitySourceType(name="url")
        aaaaa.save()

        bitbucket = GitService(name="BitBucket")
        bitbucket.save()

        github = GitService(name="GitHub")
        github.save()

        # main superuser. create it this way or "manage.py createsuperuser"
        b = User.objects.create_user("dp.test.semmler@gmail.com", email="dp.test.semmler@gmail.com",
                                     password="sherito",
                                     is_staff=True)
        b.is_active = True
        b.is_superuser = True
        b.save()
        # ^^ end of needed static data

        # testing data vvv
        board = Dashboard(name="Board 1", desc="", gkey="0B-yr9Qly_NWkUDdqMDlHaEM1RWc", user_id=1)
        board.save()

        e = GDriveTrackedFolder(name='DP', board=board, user=b, fileID='0B-yr9Qly_NWkNHcyM3pqdl9qZmc')
        e.save()

        f = IMAPCredentials(name="Seznam", user=b)
        f.update({"username": "dp-test@seznam.cz", "password": "sherito", "host": "imap.seznam.cz", "name": "Seznam"})

        g = EmailFilter(name='filter1', board=board, credentials=f, filter_from="jirka.semmler@gmail.com",
                        filter_subject_kw="dp")
        g.save()

        h = EmailFilter(name='filter2', board=board, credentials=f, filter_from="jirka.semmler@gmail.com",
                        filter_body_kw="spectre")
        h.save()

        git = GitFilter(board=board, name='main commits', token="xxx", service=github, commit_msg_kw="test")
        git.save()

        git2 = GitFilter(board=board, name='dev commits', token="yyy", service=bitbucket, commit_msg_kw="test")
        git2.save()
