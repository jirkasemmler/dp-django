from django.core.management.base import BaseCommand

from sherito.api.email_component import EmailComponent


class Command(BaseCommand):
    """
    command line handler for checking emails by cronjob
    """
    def handle(self, *args, **options):
        mailapi = EmailComponent()  # create EmailComponent object
        mailapi.check()  # run checking emails
