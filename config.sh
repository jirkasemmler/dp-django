#!/usr/bin/env bash

# --------- CONFIG ---------
PYTHON_BIN='python'
PCK_BIN_SEARCH='dnf search'
PCK_BIN_INSTALL='dnf install'
PIP_BIN='pip'
# --------- END OF CONFIG ---------