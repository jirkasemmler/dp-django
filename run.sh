#!/bin/bash
source ./config.sh

function printout {
    if [ $? ]; then
        echo -en "[ \e[32mOK\e[0m ] "
    else
        echo -en "[ \e[31mFailed\e[231m ] "
    fi
}

echo "" > .runsh_out

if [ $# == 0 ];then
    echo "Usage: ./run.sh reset [l] | translate | install | setcron"
fi

if [ "$1" == "reset" ];then

    if [ "$2" == "l" ];then
        rm db.sqlite3 >> .runsh_out
        printout
        echo "Removing SQLite DB"
    else
        # connecting to postgresql db withput password. It expects that there is 'trust' method for this user
        psql -U postgres << END_OF_SCRIPT
\connect sherito
DROP SCHEMA public CASCADE;
CREATE SCHEMA public
grant usage on schema public to public;
grant create on schema public to public;
END_OF_SCRIPT
        printout
        echo "Removing PostreSQL DB"
    fi

    rm sherito/migrations/00* >> .runsh_out
    printout
    echo "Removing old migrations"


    $PYTHON_BIN manage.py makemigrations >> .runsh_out
    printout
    echo "Creating new migrations"

    $PYTHON_BIN manage.py migrate >> .runsh_out
    printout
    echo "Migrations"

    ($PYTHON_BIN manage.py collectstatic <<< 'yes') >> .runsh_out
    printout
    echo "Static files"

    $PYTHON_BIN manage.py init >> .runsh_out
    printout
    echo "Creating new init Data"

fi



if [ "$1" == "translate" ];then
    $PYTHON_BIN manage.py makemessages >> .runsh_out
    printout
    echo "Creating translations"
fi

if [ "$1" == "install" ];then

    # PIP install
    # needed pckgs, but for rhel-based distros. if you are using something else, please find equivalents
    # e.g. debian based distros have -dev pckgs
    array=( "gcc" "python-devel" "PyQt4" "python-gssapi" "python-cffi" "pygobject3" "libffi-devel" "openssl-devel" "libxml2-devel" "python-lxml" "libxslt-devel")
    for i in "${array[@]}"
    do
        $PCK_BIN_SEARCH $i
        if [ $? ];then
            $PCK_BIN_INSTALL $i
            printout
            echo "Installing $i"
        fi
    done

    $PIP_BIN install -r req

    # server install
    array=( "httpd" "mod_wsgi" "python-pip" "python-devel" "gcc" "postgresql-server" "postgresql-devel" "postgresql-contrib")
    for i in "${array[@]}"
    do
        $PCK_BIN_SEARCH $i
        if [ $? ];then
            $PCK_BIN_INSTALL $i
            printout
            echo "Installing $i"
        fi
    done

    postgresql-setup initdb
    systemctl start postgresql

    echo "now I done here, you have to"
    echo "1) go to /var/lib/pgsql/data/pg_hba.conf and set the access to db"
    echo "2) go to https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-centos-7#perform-initial-postgresql-configuration"
    echo "  and a) set up postgreSQL DB - note that you should set all users as 'trust' method othwerwise you need to modify reset section"
    echo "  and b) set up django app"
    echo "3) set the WSGI connection by  https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/"

fi

if [ "$1" == "setcron" ];then

    CRONCMD=`pwd`"/manage.py email >> "`pwd`"/logs/cron_job_email.log 2>&1"  # adding gmail cron
    CRONJOB="* * * * * $CRONCMD"  # every minute
    cat <(crontab -l) <(echo "$CRONJOB") | crontab -

    CRONCMD2=`pwd`"/manage.py gdrive >> "`pwd`"/logs/cron_job_gdrive.log 2>&1"  # adding email cron
    CRONJOB2="* * * * * $CRONCMD2"  # every minute
    cat <(crontab -l) <(echo "$CRONJOB2") | crontab -

    service crond restart # realoading cron deamon with new data

    printout
    echo "Setting CRON"
fi
